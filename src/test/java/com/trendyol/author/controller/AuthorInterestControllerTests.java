package com.trendyol.author.controller;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.service.author.AuthorService;
import com.trendyol.author.service.interest.InterestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthorInterestControllerTests {

    @Mock
    AuthorService authorService;
    @Mock
    InterestService interestService;

    @InjectMocks
    AuthorInterestController authorInterestController;


    @Test // 200 OK
    public void it_should_return_author_interests_for_get_author_interest(){

        //Arrange
        when(authorService.findInterestsById("1")).thenReturn(Collections.singletonList(""));

        //Act
        ResponseEntity<List<String>> entity = authorInterestController.getAuthorInterest("1");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test // 201 CREATED
    public void it_should_add_interest_to_author_interests_for_add_interest(){

        //Arrange
        List<String> names = new ArrayList<>();
        when(authorService.addInterestToAuthorInterestList("1", names)).thenReturn(Collections.singletonList(""));

        //Act
        ResponseEntity<Void> entity = authorInterestController.addInterest(names,"1", "");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_document_not_found_exception_for_add_interest(){

        //Arrange
        List<String> names = new ArrayList<>();
        when(authorService.addInterestToAuthorInterestList("1", names))
                .thenThrow(new DocumentNotFoundException(null));

        //Act
        ResponseEntity<Void> entity = authorInterestController.addInterest(names,"1", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 200 OK
    public void it_should_delete_interest_from_author_interests_for_delete_interest(){

        //Arrange
        List<String> names = new ArrayList<>();
        when(authorService.deleteInterestFromAuthorInterestList("1", names)).thenReturn(Collections.singletonList(""));

        //Act
        ResponseEntity<Void> entity = authorInterestController.deleteInterest("1",names, "");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_document_not_found_exception_for_delete_interest(){

        //Arrange
        List<String> names = new ArrayList<>();
        when(authorService.deleteInterestFromAuthorInterestList("1", names))
                .thenThrow(new DocumentNotFoundException(null));

        //Act
        ResponseEntity<Void> entity = authorInterestController.deleteInterest("1",names, "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }
}
