package com.trendyol.author.controller;


import com.trendyol.author.model.article.Article;
import com.trendyol.author.service.article.ArticleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ArticleSearchControllerTests {

    @Mock
    ArticleService service;

    @InjectMocks
    ArticleSearchController controller;

    @Test // 200 OK
    public void it_should_return_200_when_article_searching_for_getArticles(){
        //Arrange
        when(service.getArticlesByFiltering("title","userName",1L,"tag"))
                .thenReturn(new ArrayList<>());

        //Act
        ResponseEntity<List<Article>> entity = controller.getArticles("title", "userName", 1L, "tag");

        //Assert
        assertEquals(HttpStatus.OK,entity.getStatusCode());

    }


}
