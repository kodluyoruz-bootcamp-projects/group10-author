package com.trendyol.author.controller;


import com.trendyol.author.common.exception.*;
import com.trendyol.author.contract.request.AddCommentRequest;
import com.trendyol.author.model.article.Comment;
import com.trendyol.author.service.comment.CommentService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommentControllerTests {
    @Mock
    CommentService service;

    @InjectMocks
    CommentController commentController;

    @Test
    public void it_should_be_200_with_getComments_controller(){


        ResponseEntity<List<Comment>> entity= commentController.getComments("1");

        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test
    public void it_should_be_ArticleNotFoundExcepiton_for_getComments(){
        when(service.getComments("1")).thenThrow(new ArticleNotFoundExcepiton(""));

        //Act
        ResponseEntity<List<Comment>> entity = commentController.getComments("1");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_200_with_getComment_controller(){


        ResponseEntity<Comment> entity= commentController.getComment("2","2");

        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test
    public void it_should_be_ArticleNotFoundExcepiton_for_getComment(){
        when(service.getComment("3","3")).thenThrow(new ArticleNotFoundExcepiton(""));

        ResponseEntity<Comment> entity= commentController.getComment("3","3");

        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_CommentNotFounException_for_getComment(){
        when(service.getComment("3","3")).thenThrow(new CommentNotFounException(""));

        ResponseEntity<Comment> entity= commentController.getComment("3","3");

        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_200_with_addComment_controller(){

        ResponseEntity<String> entity= commentController.addComment("4",new AddCommentRequest(),"4");

        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }
    @Test
    public void it_should_be_ArticleNotFoundExcepiton_as_404_with_addComment_controller(){

        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setAuthorId("55");
        addCommentRequest.setComment("merhabalar");

        doThrow(new ArticleNotFoundExcepiton("")).when(service).addComment("54",addCommentRequest);

        ResponseEntity<String> entity= commentController.addComment("54",addCommentRequest,"55");

        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_AuthorNotFoundException_as_404_with_addComment_controller(){

        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setAuthorId("55");
        addCommentRequest.setComment("merhabalar");

        doThrow(new AuthorNotFoundException("")).when(service).addComment("54",addCommentRequest);

        ResponseEntity<String> entity= commentController.addComment("54",addCommentRequest,"55");

        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_200_with_updateComment_controller(){

        ResponseEntity<URI> entity= commentController.updateComment("5",new AddCommentRequest(),"5","5");

        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test
    public void it_should_be_ArticleNotFoundExcepiton_with_updateComment_controller() throws URISyntaxException {

        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setComment("merhaba");
        addCommentRequest.setAuthorId("5");

        doThrow(new ArticleNotFoundExcepiton("")).when(service).patchComment("5","5",addCommentRequest);

        ResponseEntity<URI> entity= commentController.updateComment("5",addCommentRequest,"5","5");

        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_CommentNotFounException_with_updateComment_controller() throws URISyntaxException {

        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setComment("merhaba");
        addCommentRequest.setAuthorId("55");

        doThrow(new CommentNotFounException("")).when(service).patchComment("5","5",addCommentRequest);

        ResponseEntity<URI> entity= commentController.updateComment("5",addCommentRequest,"5","5");

        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_AuthorNotFoundException_with_updateComment_controller() throws URISyntaxException {

        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setComment("merhaba");
        addCommentRequest.setAuthorId("55");

        doThrow(new AuthorNotFoundException("")).when(service).patchComment("5","5",addCommentRequest);

        ResponseEntity<URI> entity= commentController.updateComment("5",addCommentRequest,"5","5");

        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_delete_with_deleteComment_method(){
        ResponseEntity<String> responseEntity = commentController.deleteComment("5", "5", "5", "5");

        assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
    }

    @Test
    public void it_should_be_return_CommentNotDeletedException(){

        doThrow(new CommentNotDeletedException("")).when(service).deleteComment("5","5","5");
        ResponseEntity<String> responseEntity = commentController.deleteComment("5", "5", "5", "5");

        assertEquals(HttpStatus.UNAUTHORIZED,responseEntity.getStatusCode());
    }

    @Test
    public void it_should_be_return_ArticleNotFoundExcepiton(){
        doThrow(new ArticleNotFoundExcepiton("")).when(service).deleteComment("5","5","5");
        ResponseEntity<String> responseEntity = commentController.deleteComment("5", "5", "5", "5");

        assertEquals(HttpStatus.NOT_FOUND,responseEntity.getStatusCode());
    }

    @Test
    public void it_should_be_return_AuthorNotFoundException(){
        doThrow(new AuthorNotFoundException("")).when(service).deleteComment("5","5","5");
        ResponseEntity<String> responseEntity = commentController.deleteComment("5", "5", "5", "5");

        assertEquals(HttpStatus.NOT_FOUND,responseEntity.getStatusCode());
    }

    @Test
    public void it_should_be_return_CommentNotFounException(){
        doThrow(new CommentNotFounException("")).when(service).deleteComment("5","5","5");
        ResponseEntity<String> responseEntity = commentController.deleteComment("5", "5", "5", "5");

        assertEquals(HttpStatus.NOT_FOUND,responseEntity.getStatusCode());
    }




}
