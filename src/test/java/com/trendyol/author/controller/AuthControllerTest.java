package com.trendyol.author.controller;


import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.exception.AuthAlreadyExistException;
import com.trendyol.author.common.exception.SignInFailureException;
import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.contract.request.PatchAuthRequest;
import com.trendyol.author.service.auth.AuthService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthControllerTest {

    @Mock
    AuthService service;

    @InjectMocks
    AuthController controller;

    @Test //201 CREATED
    public void it_should_return_201_when_auth_and_author_created_for_signUp() {
        //Arrange
        AddAuthRequest addAuthRequest = new AddAuthRequest("userName", "test@gmail.com",
                "password123");
        when(service.createAuth(addAuthRequest)).thenReturn("Token");

        //Act
        ResponseEntity<String> entity = controller.signUp(addAuthRequest);

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test //200 CREATED
    public void it_should_return_200_when_email_and_password_is_correct_for_signIn() {

        //Arrange
        when(service.signIn("email", "password")).thenReturn("token");

        //Act
        ResponseEntity<String> entity = controller.signIn("email", "password");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());

    }

    @Test //409 CONFLICT
    public void it_should_throw_409_when_auth_already_exist_for_signUp() {
        //Arrange
        AddAuthRequest addAuthRequest = new AddAuthRequest("userName", "test@gmail.com",
                "password123");
        when(service.createAuth(addAuthRequest)).thenThrow(new AuthAlreadyExistException(""));

        //Act
        ResponseEntity<String> entity = controller.signUp(addAuthRequest);

        //Assert
        assertEquals(HttpStatus.CONFLICT, entity.getStatusCode());
    }

    @Test //401 UNAUTHORIZED
    public void it_should_throw_401_when_auth_not_exist_for_signIp() {
        //Arrange
        when(service.signIn("email", "password"))
                .thenThrow(new SignInFailureException(""));

        //Act
        ResponseEntity<String> entity = controller.signIn("email", "password");

        //Assert
        assertEquals(HttpStatus.UNAUTHORIZED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_auth_not_found_for_signIp() {
        //Arrange
        when(service.signIn("email", "password"))
                .thenThrow(new DocumentNotFoundException(null));

        //Act
        ResponseEntity<String> entity = controller.signIn("email", "password");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //200 OK
    public void it_should_return_200_when_email_or_password_is_changed_for_updateUser() {

        //Arrange
        PatchAuthRequest auth = new PatchAuthRequest("newemail@gmail.com", "123456789");
        when(service.checkEmailAndPassword( "oldemail@gmail.com", "123456789")).thenReturn(true);

        //Act
        ResponseEntity<String> entity = controller
                .updateUser(auth, "newemail@gmail.com", "123456789", "123456", "");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());

    }

    @Test //401 UNAUTHORIZED
    public void it_should_throw_401_when_auth_not_exist_for_updateUser() {
        //Arrange
        PatchAuthRequest auth = new PatchAuthRequest("newemail@gmail.com", "123456789");
        when(service.checkEmailAndPassword( "oldemail@gmail.com", "123456789"))
                .thenThrow(new SignInFailureException(""));

        //Act
        ResponseEntity<String> entity = controller
                .updateUser(auth, "oldemail@gmail.com", "123456789", "123456789", "");

        //Assert
        assertEquals(HttpStatus.UNAUTHORIZED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_auth_not_found_for_updateUser() {
        //Arrange
        PatchAuthRequest auth = new PatchAuthRequest("newemail@gmail.com", "123456789");
        when(service.checkEmailAndPassword( "oldemail@gmail.com", "123456789"))
                .thenThrow(new DocumentNotFoundException(null));

        //Act
        ResponseEntity<String> entity = controller
                .updateUser(auth, "oldemail@gmail.com", "123456789", "123456", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //200 OK
    public void it_should_return_200_when_email_or_password_is_changed_for_deleteUser() {

        //Arrange
        when(service.checkEmailAndPassword( "oldemail@gmail.com", "123456789")).thenReturn(true);

        //Act
        ResponseEntity<String> entity = controller
                .deleteUser("newemail@gmail.com", "123456789", "123456", "");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());

    }

    @Test //401 UNAUTHORIZED
    public void it_should_throw_401_when_auth_not_exist_for_deleteUser() {
        //Arrange
        when(service.checkEmailAndPassword( "oldemail@gmail.com", "123456789"))
                .thenThrow(new SignInFailureException(""));

        //Act
        ResponseEntity<String> entity = controller
                .deleteUser("oldemail@gmail.com", "123456789", "123456", "");

        //Assert
        assertEquals(HttpStatus.UNAUTHORIZED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_auth_not_found_for_deleteUser() {
        //Arrange
        when(service.checkEmailAndPassword( "oldemail@gmail.com", "123456789"))
                .thenThrow(new DocumentNotFoundException(null));

        //Act
        ResponseEntity<String> entity = controller
                .deleteUser("oldemail@gmail.com", "123456789", "123456", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }
}