package com.trendyol.author.controller;

import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.model.article.Statistic;
import com.trendyol.author.service.statistic.StatisticService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StatisticControllerTests {

    @Mock
    private StatisticService statisticService;

    @InjectMocks
    private StatisticController controller;

    @Test //200 OK
    public void it_should_return_200_when_author_id_find_for_getStatisticsList(){
        //Arrange
        when(statisticService.getArticlesStatistic("123")).thenReturn(new ArrayList<>());

        //Act
        ResponseEntity<List<Statistic>> entity = controller.getStatisticsList("-", "123");

        //Assert
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void  it_should_return_404_when_author_id_not_found_for_getStatisticsList(){
        //Arrange
        when(statisticService.getArticlesStatistic("123")).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<List<Statistic>> entity = controller.getStatisticsList("-", "123");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test //200 OK
    public void it_should_return_200_when_author_and_article_id_find_for_getStatistic(){
        //Arrange
        when(statisticService.getArticleStatistic("123","111"))
                .thenReturn(new Statistic());

        //Act
        ResponseEntity<Statistic> entity = controller.getStatistic("-", "123", "111");

        //Assert
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void  it_should_return_404_when_author_id_not_found_for_getStatistic(){
        //Arrange
        when(statisticService.getArticleStatistic("123","111"))
                .thenThrow(new AuthorNotFoundException(""));
        //Act
        ResponseEntity<Statistic> entity = controller.getStatistic("-", "123", "111");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void  it_should_return_404_when_article_id_not_found_for_getStatistic(){
        //Arrange
        when(statisticService.getArticleStatistic("123","111"))
                .thenThrow(new ArticleNotFoundExcepiton(""));
        //Act
        ResponseEntity<Statistic> entity = controller.getStatistic("-", "123", "111");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

}
