package com.trendyol.author.controller;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.model.author.Follow;
import com.trendyol.author.service.author.AuthorService;
import com.trendyol.author.service.follow.FollowService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FollowControllerTests {

    @Mock
    AuthorService authorService;
    @Mock
    FollowService followService;

    @InjectMocks
    FollowController followController;


    @Test // 200 OK
    public void it_should_return_author_follower_list_for_get_authors_followers(){

        //Arrange
        when(followService.getUserFollowers("name")).thenReturn(Collections.singletonList(new Follow()));

        //Act
        ResponseEntity<List<Follow>> entity = followController.getAuthorsFollowers("name");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_author_not_found_for_get_author_followers() {
        //Arrange
        when(followService.getUserFollowers("name")).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<List<Follow>> entity = followController.getAuthorsFollowers("name");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 200 OK
    public void it_should_return_author_following_list_for_get_authors_followings(){

        //Arrange
        when(followService.getUserFollowings("name")).thenReturn(Collections.singletonList(new Follow()));

        //Act
        ResponseEntity<List<Follow>> entity = followController.getAuthorsFollowings("name");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_author_not_found_for_get_author_followings() {
        //Arrange
        when(followService.getUserFollowings("name")).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<List<Follow>> entity = followController.getAuthorsFollowings("name");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 201 CREATED
    public void it_should_add_follower_to_author_follower_list_for_follow_author() throws URISyntaxException {

        //Arrange
        when(followService.followAuthorByFollowUserName("username", "name")).thenReturn(new URI(""));
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("username")).thenReturn(true);

        //Act
        ResponseEntity<URI> entity = followController.followAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_author_not_found_for_follow_author() throws URISyntaxException {

        //Arrange
        when(followService.followAuthorByFollowUserName("username", "name")).thenReturn(new URI(""));
        when(authorService.existByName("name")).thenReturn(false);
        when(authorService.existByName("username")).thenReturn(true);

        //Act
        ResponseEntity<URI> entity = followController.followAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_follow_user_not_found_for_follow_author() throws URISyntaxException {

        //Arrange
        when(followService.followAuthorByFollowUserName("username", "name")).thenReturn(new URI(""));
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("username")).thenReturn(false);

        //Act
        ResponseEntity<URI> entity = followController.followAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 400 BAD_REQUEST
    public void it_should_throw_bad_request_when_user_try_to_follow_themselves_for_follow_author() throws URISyntaxException {

        //Arrange
        when(followService.followAuthorByFollowUserName("name", "name")).thenReturn(new URI(""));
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("name")).thenReturn(true);

        //Act
        ResponseEntity<URI> entity = followController.followAuthor("name", "name", "");

        //Assert
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_document_not_found_for_follow_author() throws URISyntaxException {

        //Arrange
        when(followService.followAuthorByFollowUserName("username", "name"))
                .thenThrow(new DocumentNotFoundException(null));
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("username")).thenReturn(true);

        //Act
        ResponseEntity<URI> entity = followController.followAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 500 INTERNAL_SERVER_ERROR
    public void it_should_throw_uri_syntax_exception_for_follow_author() throws URISyntaxException {

        //Arrange
        when(followService.followAuthorByFollowUserName("username", "name"))
                .thenThrow(new URISyntaxException("", ""));
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("username")).thenReturn(true);

        //Act
        ResponseEntity<URI> entity = followController.followAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }

    @Test // 201 CREATED
    public void it_should_delete_follower_from_author_follower_list_for_unfollow_author(){

        //Arrange
        doNothing().when(followService).unfollowAuthorByFollowUserName("username", "name");
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("username")).thenReturn(true);

        //Act
        ResponseEntity<Void> entity = followController.unfollowAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_author_not_found_for_unfollow_author(){

        //Arrange
        doNothing().when(followService).unfollowAuthorByFollowUserName("username", "name");
        when(authorService.existByName("name")).thenReturn(false);
        when(authorService.existByName("username")).thenReturn(true);

        //Act
        ResponseEntity<Void> entity = followController.unfollowAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_follow_user_not_found_for_unfollow_author() {

        //Arrange
        doNothing().when(followService).unfollowAuthorByFollowUserName("username", "name");
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("username")).thenReturn(false);

        //Act
        ResponseEntity<Void> entity = followController.unfollowAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 400 BAD_REQUEST
    public void it_should_throw_bad_request_when_user_try_to_unfollow_themselves_for_unfollow_author() {

        //Arrange
        doNothing().when(followService).unfollowAuthorByFollowUserName("username", "name");
        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("name")).thenReturn(true);

        //Act
        ResponseEntity<Void> entity = followController.unfollowAuthor("name", "name", "");

        //Assert
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test // 404 NOT_FOUND
    public void it_should_throw_document_not_found_for_unfollow_author() {

        //Arrange
        doThrow(new DocumentNotFoundException(null))
                .when(followService).unfollowAuthorByFollowUserName("username", "name");

        when(authorService.existByName("name")).thenReturn(true);
        when(authorService.existByName("username")).thenReturn(true);

        //Act
        ResponseEntity<Void> entity = followController.unfollowAuthor("username", "name", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }
}
