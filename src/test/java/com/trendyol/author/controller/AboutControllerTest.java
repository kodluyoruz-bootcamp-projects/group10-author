package com.trendyol.author.controller;

import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.JsonParseExceptionWithJackson;
import com.trendyol.author.model.author.About;
import com.trendyol.author.service.about.AboutService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AboutControllerTest {
    @Mock
    AboutService service;

    @InjectMocks
    AboutController aboutController;

    @Test // 404 NOT FOUND
    public void  it_should_return_404_when_author_id_not_found_for_getAbout(){
        //Arrange
        when(service.getAbout("111")).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<About> entity = aboutController.getAbout("111", "-");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test // 200 OK
    public void it_should_return_200_when_about_id_find_for_getAbout(){
        //Arrange
        when(service.getAbout("123")).thenReturn(new About());

        //Act
        ResponseEntity<About> entity = aboutController.getAbout("123","-");

        //Assert
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }


    @Test // 404 NOT FOUND
    public void  it_should_return_404_when_author_id_not_found_for_updateAbout(){
        //Arrange
        when(service.updateAbout("111",new About())).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<URI> entity = aboutController.updateAbout("111", new About(), "-");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test // 500 INTERNAL_SERVER_ERROR
    public void  it_should_return_500_when_json_parsing_problem_for_updateAbout(){
        //Arrange
        when(service.updateAbout("111",new About())).thenThrow(new JsonParseExceptionWithJackson(""));

        //Act
        ResponseEntity<URI> entity = aboutController.updateAbout("111", new About(), "-");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,entity.getStatusCode());
    }

    @Test // 200 OK
    public void  it_should_return_200_when_author_id_find_and_about_not_null_for_updateAbout()
            throws URISyntaxException {
        //Arrange
        when(service.updateAbout("111",new About())).thenReturn(new URI("author/111/about"));

        //Act
        ResponseEntity<URI> entity = aboutController.updateAbout("111", new About(), "-");

        //Assert
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }


}
