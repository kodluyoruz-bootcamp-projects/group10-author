package com.trendyol.author.controller;

import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.contract.request.AddArticleRequest;
import com.trendyol.author.contract.request.PatchArticleRequest;
import com.trendyol.author.model.article.Article;
import com.trendyol.author.service.article.ArticleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthorArticleControllerTests {

    @Mock
    ArticleService articleService;

    @InjectMocks
    AuthorArticleController authorArticleController;

    @Test // 200 OK
    public void it_should_return_authors_article_list_for_get_author_articles() {
        //Arrange
        when(articleService.getArticles("1")).thenReturn(Collections.singletonList(new Article()));

        //Act
        ResponseEntity<List<Article>> entity = authorArticleController.getAuthorArticles("1");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_author_not_found_for_get_author_articles() {
        //Arrange
        when(articleService.getArticles("1")).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<List<Article>> entity = authorArticleController.getAuthorArticles("1");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 200 OK
    public void it_should_return_author_article_by_id_for_get_author_article() {
        //Arrange
        when(articleService.getArticle("1", "2")).thenReturn(new Article());

        //Act
        ResponseEntity<Article> entity = authorArticleController.getAuthorArticle("1","2");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_author_not_found_for_get_author_article() {
        //Arrange
        when(articleService.getArticle("1", "2")).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<Article> entity = authorArticleController.getAuthorArticle("1","2");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_article_not_found_for_get_author_article() {
        //Arrange
        when(articleService.getArticle("1", "2")).thenThrow(new ArticleNotFoundExcepiton(""));

        //Act
        ResponseEntity<Article> entity = authorArticleController.getAuthorArticle("1","2");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 201 CREATED
    public void it_should_return_added_article_uri_for_add_article() throws URISyntaxException {
        //Arrange
        AddArticleRequest article = new AddArticleRequest();
        when(articleService.insert("1", article)).thenReturn(URI.create("uri"));

        //Act
        ResponseEntity<URI> entity = authorArticleController.addArticle(article,"1", "");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_author_not_found_for_add_article() throws URISyntaxException {
        //Arrange
        AddArticleRequest article = new AddArticleRequest();
        when(articleService.insert("1", article)).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<URI> entity = authorArticleController.addArticle(article,"1", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //500 INTERNAL_SERVER_ERROR
    public void it_should_throw_500_when_uri_exception_exist_for_add_article() throws URISyntaxException {
        //Arrange
        AddArticleRequest article = new AddArticleRequest();
        when(articleService.insert("1", article)).thenThrow(new URISyntaxException("",""));

        //Act
        ResponseEntity<URI> entity = authorArticleController.addArticle(article,"1", "");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }

    @Test // 200 OK
    public void it_should_delete_article_for_delete_article() {

        //Act
        ResponseEntity<Void> entity = authorArticleController.deleteArticle("1","2", "");

        //Assert
        assertEquals(HttpStatus.OK, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_author_not_found_for_delete_article() {
        //Arrange
        doThrow(new AuthorNotFoundException("")).when(articleService).delete("1","2");

        //Act
        ResponseEntity<Void> entity = authorArticleController.deleteArticle("1","2", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_article_not_found_for_delete_article() {
        //Arrange
        doThrow(new ArticleNotFoundExcepiton("")).when(articleService).delete("1","2");

        //Act
        ResponseEntity<Void> entity = authorArticleController.deleteArticle("1","2", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test // 201 CREATED
    public void it_should_return_updated_article_uri_for_update_article() throws URISyntaxException {
        //Arrange
        PatchArticleRequest article = new PatchArticleRequest();
        when(articleService.update("1", "2", article)).thenReturn(URI.create("uri"));

        //Act
        ResponseEntity<URI> entity = authorArticleController.updateArticle(article,"1", "2", "");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_throw_404_when_author_not_found_for_update_article() throws URISyntaxException {
        //Arrange
        PatchArticleRequest article = new PatchArticleRequest();
        when(articleService.update("1", "2", article)).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<URI> entity = authorArticleController.updateArticle(article,"1", "2", "");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //500 INTERNAL_SERVER_ERROR
    public void it_should_throw_500_when_uri_exception_exist_for_update_article() throws URISyntaxException {
        //Arrange
        PatchArticleRequest article = new PatchArticleRequest();
        when(articleService.update("1", "2", article)).thenThrow(new URISyntaxException("",""));

        //Act
        ResponseEntity<URI> entity = authorArticleController.updateArticle(article,"1", "2", "");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }
}
