package com.trendyol.author.controller;

import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.StorageUploadFailed;
import com.trendyol.author.service.storage.StorageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StorageControllerTests {

    @Mock
    private StorageService storageService;

    @InjectMocks
    private StorageController controller;


    private final MockMultipartFile firstFile = new MockMultipartFile("data",
            "filename.jpg", "image/", "some jpg".getBytes());

    @Test //201 CREATED
    public void it_should_return_201_when_uploaded_profile_photo_for_uploadProfilePhoto() {
        //Arrange
        doNothing().when(storageService).uploadProfileImage(firstFile, "123");

        //Act
        ResponseEntity<String> entity = controller.uploadProfilePhoto(firstFile, "-", "123");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_return_404_when_author_id_not_found_for_uploadProfilePhoto() {
        //Arrange
             doThrow(AuthorNotFoundException.class).when(storageService).uploadProfileImage(firstFile, "123");

        //Act
        ResponseEntity<String> entity = controller.uploadProfilePhoto(firstFile, "-", "123");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //500 INTERNAL_SERVER_ERROR
    public void it_should_return_500_when_any_storage_problem_for_uploadProfilePhoto() {
        //Arrange
        doThrow(StorageUploadFailed.class).when(storageService).uploadProfileImage(firstFile, "123");

        //Act
        ResponseEntity<String> entity = controller.uploadProfilePhoto(firstFile, "-", "123");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }

    @Test //201 CREATED
    public void it_should_return_201_when_uploaded_article_feature_photo_for_uploadArticleFeaturedImage() {
        //Arrange
        doNothing().when(storageService).uploadFeaturedImage(firstFile, "123", "111");

        //Act
        ResponseEntity<String> entity = controller.uploadArticleFeaturedImage(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_return_404_when_author_id_not_found_for_uploadArticleFeaturedImage() {
        //Arrange
        doThrow(AuthorNotFoundException.class).when(storageService).uploadFeaturedImage(firstFile, "123","111");

        //Act
        ResponseEntity<String> entity = controller.uploadArticleFeaturedImage(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_return_404_when_article_id_not_found_for_uploadArticleFeaturedImage() {
        //Arrange
        doThrow(ArticleNotFoundExcepiton.class).when(storageService).uploadFeaturedImage(firstFile, "123","111");

        //Act
        ResponseEntity<String> entity = controller.uploadArticleFeaturedImage(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //500 INTERNAL_SERVER_ERROR
    public void it_should_return_500_when_any_storage_problem_for_uploadArticleFeaturedImage() {
        //Arrange
        doThrow(StorageUploadFailed.class).when(storageService).uploadFeaturedImage(firstFile, "123","111");

        //Act
        ResponseEntity<String> entity = controller.uploadArticleFeaturedImage(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }

    @Test //201 CREATED
    public void it_should_return_201_when_uploaded_article_feature_photo_for_uploadArticleContentFile() {
        //Arrange
        when(storageService.uploadArticleContentFile(firstFile, "123", "111"))
                .thenReturn("url");

        //Act
        ResponseEntity<String> entity = controller.uploadArticleContentFile(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_return_404_when_author_id_not_found_for_uploadArticleContentFile() {
        //Arrange
        when(storageService.uploadArticleContentFile(firstFile, "123", "111"))
                .thenThrow(new AuthorNotFoundException(""));
        //Act
        ResponseEntity<String> entity = controller.uploadArticleContentFile(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void it_should_return_404_when_article_id_not_found_for_uploadArticleContentFile() {
        //Arrange
        //Arrange
        when(storageService.uploadArticleContentFile(firstFile, "123", "111"))
                .thenThrow(new ArticleNotFoundExcepiton(""));
        //Act
        ResponseEntity<String> entity = controller.uploadArticleContentFile(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test //500 INTERNAL_SERVER_ERROR
    public void it_should_return_500_when_any_storage_problem_for_uuploadArticleContentFile() {
        //Arrange
        //Arrange
        when(storageService.uploadArticleContentFile(firstFile, "123", "111"))
                .thenThrow(new StorageUploadFailed(""));
        //Act
        ResponseEntity<String> entity = controller.uploadArticleContentFile(firstFile, "-", "123", "111");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, entity.getStatusCode());
    }
}
