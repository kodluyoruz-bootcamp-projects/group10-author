package com.trendyol.author.controller;

import org.junit.Test;


public class MonitoringControllerTests {
    MonitoringController  controller = new MonitoringController();

    @Test
    public void  live_control(){
        assert controller.live().equals("OK");
    }

    @Test
    public void  ready_control(){
        assert controller.ready().equals("OK");
    }
}
