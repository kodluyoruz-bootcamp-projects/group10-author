package com.trendyol.author.controller;

import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.CommentNotFounException;
import com.trendyol.author.model.article.Clap;
import com.trendyol.author.model.author.About;
import com.trendyol.author.service.statistic.StatisticService;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ClapControllerTests {
    @Mock
    StatisticService service;

    @InjectMocks
    ClapsController clapsController;

    @Test
    public void return_status200_get_claps_with_getClaps(){
        //Arrange
        when(service.getArticleClapSize("1")).thenReturn(new ArrayList<>());

        //Act
        ResponseEntity<Integer> entity = clapsController.getClaps("1");

        //Assert
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test
    public void return_ArticleNotFoundExcepiton_with_getClaps(){
        //Arrange
        when(service.getArticleClapSize("1")).thenThrow(new ArticleNotFoundExcepiton(""));

        //Act
        ResponseEntity<Integer> entity = clapsController.getClaps("1");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_crated_as_status_code_200()throws URISyntaxException {
        //Arrange
        when(service.clapArticle("2","2")).thenReturn(new URI("/sda/sad/"));

        //Act
        ResponseEntity<URI> entity = clapsController.addClap("2","2","2");

        //Assert
        assertEquals(HttpStatus.CREATED,entity.getStatusCode());
    }

    @Test
    public void it_should_be_ArticleNotFoundExcepiton_for_addClap()throws URISyntaxException {
        //Arrange
        when(service.clapArticle("2","2")).thenThrow(new ArticleNotFoundExcepiton(""));

        //Act
        ResponseEntity<URI> entity = clapsController.addClap("2","2","2");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_AuthorNotFoundException_for_addClap()throws URISyntaxException {
        //Arrange
        when(service.clapArticle("2","2")).thenThrow(new AuthorNotFoundException(""));

        //Act
        ResponseEntity<URI> entity = clapsController.addClap("2","2","2");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }
    @Test
    public void it_should_be_URISyntaxException_for_addClap()throws URISyntaxException {
        //Arrange
        when(service.clapArticle("2","2")).thenThrow(new URISyntaxException("ss","ss"));

        //Act
        ResponseEntity<URI> entity = clapsController.addClap("2","2","2");

        //Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,entity.getStatusCode());
    }

    @Test
    public void it_should_be_return_200(){
        //Arrange
        //Act
        ResponseEntity<Void> entity = clapsController.deleteClap("3","3","3");

        //Assert
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test
    public void it_should_be_ArticleNotFoundExcepiton_for_deleteClap(){
        //Arrange
        doThrow(new ArticleNotFoundExcepiton("")).when(service).deleteClapFromArticle("5","5");

        //Act
        ResponseEntity<Void> entity = clapsController.deleteClap("5","5","5");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test
    public void it_should_be_AuthorNotFoundException_for_deleteClap(){
        //Arrange
        doThrow(new AuthorNotFoundException("")).when(service).deleteClapFromArticle("5","5");

        //Act
        ResponseEntity<Void> entity = clapsController.deleteClap("5","5","5");

        //Assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }


}
