package com.trendyol.author.controller;


import com.couchbase.client.core.error.CouchbaseException;
import com.trendyol.author.common.exception.InterestNotCreatedException;
import com.trendyol.author.common.exception.InterestNotFoundException;
import com.trendyol.author.model.interest.Interest;
import com.trendyol.author.service.interest.InterestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class InterestControllerTests {

    @Mock
    private  InterestService interestService;

    @InjectMocks
    private InterestController controller;

    @Test //200 OK
    public void  it_should_return_200_and_interest_list_when_call_function_for_getInterests(){
        //Arrange
        when(interestService.findAll()).thenReturn(new ArrayList<>());

        //Act
        ResponseEntity<List<Interest>> entity = controller.getInterests();

        //assert
        assertNotNull(entity.getBody());
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test //200 OK
    public void  it_should_return_200_and_interest_when_call_function_for_getInterest(){
        //Arrange
        when(interestService.findById("123")).thenReturn(new Interest());

        //Act
        ResponseEntity<Interest> entity = controller.getInterest("123");

        //assert
        assertNotNull(entity.getBody());
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }

    @Test //404 NOT_FOUND
    public void  it_should_return_404_when_interest_id_not_found_for_getInterest(){
        //Arrange
        when(interestService.findById("123")).thenThrow(new InterestNotFoundException(""));

        //Act
        ResponseEntity<Interest> entity = controller.getInterest("123");

        //assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }

    @Test //201 CREATED
    public void  it_should_return_201_and_new_url_when_interest_name_added_for_addInterest() throws URISyntaxException {
        //Arrange
        when(interestService.addInterest("name")).thenReturn(new URI("/name"));

        //Act
        ResponseEntity<URI> entity = controller.addInterest("name");

        //assert
        assertNotNull(entity.getBody());
        assertEquals(HttpStatus.CREATED,entity.getStatusCode());
    }

    @Test //500 INTERNAL_SERVER_ERROR
    public void  it_should_return_500_when_interest_name_not_created_for_addInterest() throws URISyntaxException {
        //Arrange
        when(interestService.addInterest("name")).thenThrow(new InterestNotCreatedException(""));

        //Act
        ResponseEntity<URI> entity = controller.addInterest("name");

        //assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,entity.getStatusCode());
    }

    @Test //500 INTERNAL_SERVER_ERROR
    public void  it_should_return_500_when_any_url_problem_for_addInterest() throws URISyntaxException {
        //Arrange
        when(interestService.addInterest("name")).thenThrow(new URISyntaxException("",""));

        //Act
        ResponseEntity<URI> entity = controller.addInterest("name");

        //assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,entity.getStatusCode());
    }

    @Test //200 OK
    public void  it_should_return_200_and_url_when_updated_interest_for_updateInterest() throws URISyntaxException {
        //Arrange
        when(interestService.updateInterest("name","123")).thenReturn(new URI("/"));

        //Act
        ResponseEntity<URI> entity = controller.updateInterest("name", "123");

        //assert
        assertNotNull(entity.getBody());
        assertEquals(HttpStatus.OK,entity.getStatusCode());
    }


    @Test //404 NOT_FOUND
    public void  it_should_return_404_when_interest_id_not_found_for_updateInterest() throws URISyntaxException {
        //Arrange
        when(interestService.updateInterest("name","123")).thenThrow(new InterestNotFoundException(""));

        //Act
        ResponseEntity<URI> entity = controller.updateInterest("name", "123");

        //assert
        assertEquals(HttpStatus.NOT_FOUND,entity.getStatusCode());
    }
}
