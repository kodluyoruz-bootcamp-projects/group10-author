package com.trendyol.author.model.interest;

import com.trendyol.author.common.exception.PopularityCannotBeMinusException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InterestTests {

    @Test
    public void it_should_create_new_interest_with_all_args_constructor(){

        // Arrange
        Interest interest = new Interest("4564564561564641514", "Food", 0);

        // Assert
        assertEquals(interest.getId(), "4564564561564641514");
        assertEquals(interest.getName(), "Food");
        assertEquals(interest.getPopularity(), 0);

    }

    @Test
    public void it_should_return_interest_name_when_new_interest_created(){

        // Arrange
        Interest interest = new Interest("Food");

        // Assert
        assertEquals(interest.getName(), "Food");
    }

    @Test
    public void it_should_return_popularity_as_zero_when_new_interest_created(){

        // Arrange
        Interest interest = new Interest("Food");

        // Assert
        assertEquals(interest.getPopularity(), 0);
    }

    @Test
    public void it_should_return_popularity_as_one_when_interest_added() {

        // Arrange
        Interest interest = new Interest("Culture");

        // Act
        interest.increasePopularity();

        // Assert
        assertEquals(interest.getPopularity(), 1);

    }

    @Test
    public void it_should_return_popularity_as_three_when_interest_added_three_time() {

        // Arrange
        Interest interest = new Interest("Culture");

        // Act
        for(int i = 0; i < 3; i++){
            interest.increasePopularity();
        }

        // Assert
        assertEquals(interest.getPopularity(), 3);
    }

    @Test
    public void it_should_return_popularity_as_zero_when_interest_deleted_from_a_list() {

        // Arrange
        Interest interest = new Interest("Culture");

        // Act
        interest.increasePopularity();
        interest.decreasePopularity();

        // Assert
        assertEquals(interest.getPopularity(), 0);
    }

    @Test
    public void it_should_throw_exception_when_deleted_although_its_popularity_is_zero() {

        // Arrange
        Interest interest = new Interest("Culture");

        // Assert
        Assertions.assertThrows(PopularityCannotBeMinusException.class, interest::decreasePopularity);
    }

    @Test
    public void it_should_decrease_and_return_popularity_when_interest_deleted() {

        // Arrange
        Interest interest = new Interest("Culture");

        // Act
        interest.increasePopularity();
        interest.decreasePopularity();

        // Assert
        assertEquals(interest.getPopularity(), 0);
    }

}
