package com.trendyol.author.model.auth;

import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.contract.request.PatchAuthRequest;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class AuthTests {

    @Test
    public void it_should_create_auth_add_auth_request() {

        // Arrange
        AddAuthRequest addAuthRequest = new AddAuthRequest("userName", "usermail@gmail.com", "123456789");
        Auth auth = new Auth(addAuthRequest);

        // Act
        String id = auth.getId();

        // Assert
        assertEquals(auth.getId(), id);
        assertEquals(auth.getEmail(), "usermail@gmail.com");
        assertEquals(auth.getUserName(), "userName");
        assertEquals(auth.get_password(), "123456789");
    }

    @Test
    public void it_should_patch_auth_email_when_user_update_it() {

        // Arrange
        AddAuthRequest addAuthRequest = AddAuthRequest.builder()
                .userName("userName")
                .email("usermail@gmail.com")
                ._password("123456789")
                .build();
        Auth auth = new Auth(addAuthRequest);

        PatchAuthRequest patchAuthRequest = PatchAuthRequest.builder()
                .email("usernewmail@gmail.com")
                .build();

        // Act
        auth.patch(patchAuthRequest);

        // Assert
        assertEquals(auth.getEmail(), "usernewmail@gmail.com");
    }

    @Test
    public void it_should_patch_auth_password_when_user_update_it() {

        // Arrange
        AddAuthRequest addAuthRequest = AddAuthRequest.builder()
                .userName("userName")
                .email("usermail@gmail.com")
                ._password("123456789")
                .build();
        Auth auth = new Auth(addAuthRequest);

        PatchAuthRequest patchAuthRequest = PatchAuthRequest.builder()
                ._password("1234new")
                .build();

        // Act
        auth.patch(patchAuthRequest);

        // Assert
        assertEquals(auth.get_password(), "1234new");
    }

    @Test
    public void it_should_patch_auth_password_and_email_when_user_update_auth() {

        // Arrange
        AddAuthRequest addAuthRequest = AddAuthRequest.builder()
                .userName("userName")
                .email("usermail@gmail.com")
                ._password("123456789")
                .build();
        Auth auth = new Auth(addAuthRequest);

        PatchAuthRequest patchAuthRequest = PatchAuthRequest.builder()
                ._password("1234new")
                .email("newemail@gmail.com")
                .build();

        // Act
        auth.patch(patchAuthRequest);

        // Assert
        assertEquals(auth.get_password(), "1234new");
        assertEquals(auth.getEmail(), "newemail@gmail.com");
    }
}
