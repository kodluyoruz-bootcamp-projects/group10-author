package com.trendyol.author.model.article;

import com.trendyol.author.contract.request.AddCommentRequest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Arrays;
import java.util.Date;

public class CommentTest {

    @Test
    public void bla_bla() {

        String inputComment = "Merhabalar";

        Comment comment = new Comment("id1", inputComment);

        assertThat(inputComment).isEqualTo(comment.getComment());

    }
//PATCH
    @Test
    public void it_should_be_same_comment_when_patch() {
        Comment comment = getComment();

        String newComment = "New comment";
        comment.patch(newComment);

        assertThat(comment.getComment()).isEqualTo(newComment);
    }

    @Test
    public void it_should_be_dif_comment_when_patch() {

        Comment comment = getComment();
        Long date = comment.getDate();

        String newComment = "New comment";
        comment.patch(newComment);

        assertNotEquals(date, comment.getDate());

    }


// CREATE WITH CONSTRUCTOR
    @Test
    public void it_should_be_create_with_constructor_method(){
        String authorId = "123";
        String authorComment = "Constructor test comment.";

        Comment comment = new Comment(authorId, authorComment);

        assertThat(comment.getComment()).isEqualTo(authorComment);

    }


    private Comment getComment() {
        return Comment.builder().id("123")
                .authorId("1")
                .comment("Merhaba bu benim ilk yorumum.")
                .date(1603022806972L)
                .build();
    }

}
