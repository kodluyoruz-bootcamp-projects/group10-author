package com.trendyol.author.model.article;


import com.trendyol.author.common.enums.ArticleType;
import com.trendyol.author.common.enums.PublishStatus;
import com.trendyol.author.contract.request.AddArticleRequest;
import com.trendyol.author.contract.request.PatchArticleRequest;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ArticleTests {

    @Test
    public void it_should_be_one_that_authorId_list_size_when_create_article() {
        AddArticleRequest addArticleRequest = getArticleRequest();

        Article article = new Article("1", addArticleRequest);

        assertEquals(1,article.getAuthorId().size());
    }

    @Test
    public void it_should_article_request_title_equals_to_article_title() {

        AddArticleRequest addArticleRequest = getArticleRequest();

        Article article = new Article("1", addArticleRequest);
        assertEquals(article.getTitle(), addArticleRequest.getTitle());
    }

    @Test
    public void it_should_patch_only_content_article_type_when_update_article(){

        // Arrange
        AddArticleRequest addArticleRequest = getArticleRequest();
        Article article = new Article("1521", addArticleRequest);
        Content newContent = Content.builder()
                .articleType(ArticleType.TXT)
                .build();

        // Act
        String data = article.getContent().getData();
        PatchArticleRequest patchArticleRequest = PatchArticleRequest.builder()
                .content(newContent)
                .build();

        article.patchArticle(patchArticleRequest);

        // Assert
        assertEquals(article.getContent().getArticleType(), ArticleType.TXT);
        assertEquals(article.getContent().getData(), data);
    }

    @Test
    public void it_should_patch_only_article_publish_status_when_update_article(){

        // Arrange
        AddArticleRequest addArticleRequest = getArticleRequest();
        Article article = new Article("1521", addArticleRequest);

        // Act
        String data = article.getContent().getData();
        PatchArticleRequest patchArticleRequest = PatchArticleRequest.builder()
                .publishStatus(PublishStatus.PUBLIC)
                .build();
        article.patchArticle(patchArticleRequest);

        // Assert
        assertEquals(article.getPublishStatus(), PublishStatus.PUBLIC);
        assertEquals(article.getContent().getData(), data);
    }

    @Test
    public void it_should_patch_all_the_article_when_update_article(){

        // Arrange
        AddArticleRequest addArticleRequest = getArticleRequest();
        Article article = new Article("1521", addArticleRequest);
        List<String> tags = new ArrayList<>();
        tags.add("java");

        // Act
        PatchArticleRequest patchArticleRequest = PatchArticleRequest.builder()
                .featuredImageUrl("imageUrl")
                .title("newTitle")
                .tags(tags)
                .publishStatus(PublishStatus.PUBLIC)
                .build();
        article.patchArticle(patchArticleRequest);

        // Assert
        assertEquals(article.getPublishStatus(), patchArticleRequest.getPublishStatus());
        assertEquals(article.getTitle(), patchArticleRequest.getTitle());
        assertEquals(article.getFeaturedImageUrl(), patchArticleRequest.getFeaturedImageUrl());
        assertEquals(article.getTags(), patchArticleRequest.getTags());

    }

    @Test
    public void it_should_return_clap_size_as_1_when_a_user_clap_the_article(){

        // Arrange
        AddArticleRequest addArticleRequest = getArticleRequest();
        Article article = new Article("1521", addArticleRequest);
        article.getClaps().add(new Clap("2"));

        // Assert
        assertEquals(article.getClaps().size(), 1);
        assertEquals(article.getClaps().get(0).getAuthorId(), "2");

    }

    private AddArticleRequest getArticleRequest() {
        return AddArticleRequest.builder()
                .content(new Content(ArticleType.HTML, "Hello HTML"))
                .publishStatus(PublishStatus.DRAFT)
                .title("ı learn html ")
                .tags(Arrays.asList("HTML", "PHP"))
                .featuredImageUrl("www.image.png")
                .build();
    }
}
