package com.trendyol.author.model.author;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AboutTests {

    @Test
    public void it_should_create_about_object_with_all_args_constructor(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");

        // Assert
        assertEquals(about.getUserName(), "userName");
        assertEquals(about.getName(), "name");
        assertEquals(about.getProfileUrl(), "author/userName");
        assertNull(about.getProfilePhotoUrl());
        assertEquals(about.getSocialMedia(), socialMedia);
        assertEquals(about.getBio(), "bio");
    }

    @Test
    public void it_should_create_new_about_object_with_no_args_constructor(){

        // Arrange
        About about = new About();

        // Assert
        assertNull(about.getUserName());
        assertNull(about.getName());
        assertNull(about.getBio());
        assertNull(about.getProfilePhotoUrl());
        assertNull(about.getProfileUrl());
        assertNull(about.getSocialMedia());
    }

    @Test
    public void it_should_create_new_about_object_with_builder(){

        // Arrange
        About about = About.builder()
                .bio("bio")
                .name("name")
                .userName("userName")
                .build();

        // Assert
        assertEquals(about.getBio(), "bio");
        assertEquals(about.getName(), "name");
        assertEquals(about.getUserName(), "userName");
    }

    @Test
    public void it_should_patch_only_name_when_update_abouts_name(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");
        About newAbout = new About();

        // Act
        newAbout.setName("newName");
        about.patch(newAbout);

        // Assert
        assertEquals(about.getUserName(), "userName");
        assertEquals(about.getName(), "newName");
        assertEquals(about.getProfileUrl(), "author/userName");
        assertNull(about.getProfilePhotoUrl());
        assertEquals(about.getSocialMedia(), socialMedia);
        assertEquals(about.getBio(), "bio");
    }

    @Test
    public void it_should_patch_only_profileUrl_when_update_abouts_profileUrl(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");
        About newAbout = new About();

        // Act
        newAbout.setProfileUrl("newProfileUrl");
        about.patch(newAbout);

        // Assert
        assertEquals(about.getUserName(), "userName");
        assertEquals(about.getName(), "name");
        assertEquals(about.getProfileUrl(), "newProfileUrl");
        assertNull(about.getProfilePhotoUrl());
        assertEquals(about.getSocialMedia(), socialMedia);
        assertEquals(about.getBio(), "bio");
    }

    @Test
    public void it_should_patch_only_profilePhotoUrl_when_update_abouts_profilePhotoUrl(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");
        About newAbout = new About();

        // Act
        newAbout.setProfilePhotoUrl("newProfilePhotoUrl");
        about.patch(newAbout);

        // Assert
        assertEquals(about.getUserName(), "userName");
        assertEquals(about.getName(), "name");
        assertEquals(about.getProfileUrl(), "author/userName");
        assertEquals(about.getProfilePhotoUrl(), "newProfilePhotoUrl");
        assertEquals(about.getSocialMedia(), socialMedia);
        assertEquals(about.getBio(), "bio");
    }

    @Test
    public void it_should_patch_only_social_media_when_update_abouts_social_media(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");

        SocialMedia newSocialMedia = new SocialMedia();
        About newAbout = new About();

        // Act
        newSocialMedia.setGithub("github");
        newAbout.setSocialMedia(newSocialMedia);
        about.patch(newAbout);

        // Assert
        assertEquals(about.getUserName(), "userName");
        assertEquals(about.getName(), "name");
        assertEquals(about.getProfileUrl(), "author/userName");
        assertNull(about.getProfilePhotoUrl());
        assertEquals(about.getSocialMedia().getGithub(), "github");
        assertEquals(about.getBio(), "bio");
    }

    @Test
    public void it_should_patch_only_bio_when_update_abouts_bio(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");
        About newAbout = new About();

        // Act
        newAbout.setBio("newBio");
        about.patch(newAbout);

        // Assert
        assertEquals(about.getUserName(), "userName");
        assertEquals(about.getName(), "name");
        assertEquals(about.getProfileUrl(), "author/userName");
        assertNull(about.getProfilePhotoUrl());
        assertEquals(about.getSocialMedia(), socialMedia);
        assertEquals(about.getBio(), "newBio");
    }

    @Test
    public void it_should_not_patch_about_userName_when_user_update_about_userName(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");
        About newAbout = new About();

        // Act
        newAbout.setUserName("newUsername");
        about.patch(newAbout);

        // Assert
        assertNotEquals(about.getUserName(), "newUsername");
        assertEquals(about.getName(), "name");
        assertEquals(about.getProfileUrl(), "author/userName");
        assertNull(about.getProfilePhotoUrl());
        assertEquals(about.getSocialMedia(), socialMedia);
        assertEquals(about.getBio(), "bio");
    }

    @Test
    public void it_should_patch_all_about_when_update_abouts_all_parameter(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();
        About about = new About("userName", "name", "author/userName", null, socialMedia, "bio");

        SocialMedia newSocialMedia = new SocialMedia();
        newSocialMedia.setGithub("github");
        About newAbout = new About("userName", "newName", "author/newUserName", null, newSocialMedia, "newBio");

        // Act
        about.patch(newAbout);

        // Assert
        assertEquals(about.getUserName(), "userName");
        assertEquals(about.getName(), "newName");
        assertEquals(about.getProfileUrl(), "author/newUserName");
        assertNull(about.getProfilePhotoUrl());
        assertEquals(about.getSocialMedia(), newSocialMedia);
        assertEquals(about.getBio(), "newBio");
    }
}
