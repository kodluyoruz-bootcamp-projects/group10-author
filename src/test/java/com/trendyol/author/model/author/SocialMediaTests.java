package com.trendyol.author.model.author;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SocialMediaTests {

    @Test
    public void it_should_create_a_social_media_object_with_all_args_constructor(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia("twitter", "website", "github", "linkedin");

        // Assert
        assertEquals(socialMedia.getTwitter(), "twitter");
        assertEquals(socialMedia.getWebsite(), "website");
        assertEquals(socialMedia.getGithub(), "github");
        assertEquals(socialMedia.getLinkedin(), "linkedin");
    }

    @Test
    public void it_should_create_a_social_media_object_with_no_args_constructor(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia();

        // Assert
        assertNull(socialMedia.getTwitter());
        assertNull(socialMedia.getLinkedin());
        assertNull(socialMedia.getWebsite());
        assertNull(socialMedia.getGithub());
    }

    @Test
    public void it_should_create_new_social_media_object_with_builder(){

        // Arrange
        SocialMedia socialMedia = SocialMedia.builder()
                .github("github")
                .build();

        // Assert
        assertEquals(socialMedia.getGithub(), "github");
    }

    @Test
    public void it_should_patch_only_github(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia("twitter", "website", "github", "linkedin");
        SocialMedia newSocialMedia = new SocialMedia();
        newSocialMedia.setGithub("newGithub");

        // Act
        socialMedia.patch(newSocialMedia);

        // Assert
        assertEquals(socialMedia.getTwitter(), "twitter");
        assertEquals(socialMedia.getWebsite(), "website");
        assertEquals(socialMedia.getGithub(), "newGithub");
        assertEquals(socialMedia.getLinkedin(), "linkedin");
    }

    @Test
    public void it_should_patch_only_twitter(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia("twitter", "website", "github", "linkedin");
        SocialMedia newSocialMedia = new SocialMedia();
        newSocialMedia.setTwitter("newTwitter");

        // Act
        socialMedia.patch(newSocialMedia);

        // Assert
        assertEquals(socialMedia.getTwitter(), "newTwitter");
        assertEquals(socialMedia.getWebsite(), "website");
        assertEquals(socialMedia.getGithub(), "github");
        assertEquals(socialMedia.getLinkedin(), "linkedin");
    }

    @Test
    public void it_should_patch_only_website(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia("twitter", "website", "github", "linkedin");
        SocialMedia newSocialMedia = new SocialMedia();
        newSocialMedia.setWebsite("newWebsite");

        // Act
        socialMedia.patch(newSocialMedia);

        // Assert
        assertEquals(socialMedia.getTwitter(), "twitter");
        assertEquals(socialMedia.getWebsite(), "newWebsite");
        assertEquals(socialMedia.getGithub(), "github");
        assertEquals(socialMedia.getLinkedin(), "linkedin");
    }

    @Test
    public void it_should_patch_only_linkedin(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia("twitter", "website", "github", "linkedin");
        SocialMedia newSocialMedia = new SocialMedia();
        newSocialMedia.setLinkedin("newLinkedin");

        // Act
        socialMedia.patch(newSocialMedia);

        // Assert
        assertEquals(socialMedia.getTwitter(), "twitter");
        assertEquals(socialMedia.getWebsite(), "website");
        assertEquals(socialMedia.getGithub(), "github");
        assertEquals(socialMedia.getLinkedin(), "newLinkedin");
    }

    @Test
    public void it_should_patch_only_linkedin_and_github(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia("twitter", "website", "github", "linkedin");
        SocialMedia newSocialMedia = new SocialMedia();
        newSocialMedia.setLinkedin("newLinkedin");
        newSocialMedia.setGithub("newGithub");

        // Act
        socialMedia.patch(newSocialMedia);

        // Assert
        assertEquals(socialMedia.getTwitter(), "twitter");
        assertEquals(socialMedia.getWebsite(), "website");
        assertEquals(socialMedia.getGithub(), "newGithub");
        assertEquals(socialMedia.getLinkedin(), "newLinkedin");
    }

    @Test
    public void it_should_patch_all_the_parameters(){

        // Arrange
        SocialMedia socialMedia = new SocialMedia("twitter", "website", "github", "linkedin");
        SocialMedia newSocialMedia = new SocialMedia("newTwitter", "newWebsite", "newGithub", "newLinkedin");

        // Act
        socialMedia.patch(newSocialMedia);

        // Assert
        assertEquals(socialMedia.getTwitter(), "newTwitter");
        assertEquals(socialMedia.getWebsite(), "newWebsite");
        assertEquals(socialMedia.getGithub(), "newGithub");
        assertEquals(socialMedia.getLinkedin(), "newLinkedin");
    }

}
