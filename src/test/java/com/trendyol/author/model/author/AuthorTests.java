package com.trendyol.author.model.author;

import com.trendyol.author.model.article.Article;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class AuthorTests {

    @Test
    public void it_should_create_new_author_with_id_and_userName(){

        // Arrange
        Author author = new Author("45645456", "userName");

        // Assert
        assertEquals(author.getId(), "45645456");
        assertEquals(author.getAbout().getUserName(), "userName");
    }

    @Test
    public void it_should_create_new_author_with_all_args_constructor(){

        // Arrange
        List<Follow> follower = new ArrayList<>();
        List<Follow> following = new ArrayList<>();
        List<String> interests = new ArrayList<>();
        List<Article> articles = new ArrayList<>();
        About about = new About();

        // Act
        Author author = new Author("45645456", about, articles, interests, follower, following);

        // Assert
        assertEquals(author.getId(), "45645456");
        assertEquals(author.getAbout(), about);
        assertEquals(author.getArticles(), articles);
        assertEquals(author.getFollowers(), follower);
        assertEquals(author.getFollowings(), following);
        assertEquals(author.getInterests(), interests);
    }

    @Test
    public void it_should_create_new_author_with_no_args_constructor(){

        // Arrange
        Author author = new Author();

        // Assert
        assertNull(author.getId());
    }

    @Test
    public void it_should_create_new_author_object_with_builder(){

        // Arrange
        Author newAuthor = Author.builder()
                .id("5484454")
                .build();

        // Assert
        assertEquals(newAuthor.getId(), "5484454");
    }

    @Test
    public void it_should_update_authors_interests(){

        // Arrange
        Author author = new Author("45645456", "userName");
        List<String> interests = new ArrayList<>();
        interests.add("History");

        // Act
        author.setInterests(interests);

        // Assert
        assertEquals(author.getInterests(), interests);
    }
}
