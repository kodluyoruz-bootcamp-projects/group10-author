package com.trendyol.author.model.author;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class FollowTests {

    @Test
    public void it_should_create_new_follow_object_with_all_args_constructor(){

        // Arrange
        Follow follow = new Follow("newAuthor", "author/newAuthor");

        // Assert
        assertEquals(follow.getUserName(), "newAuthor");
        assertEquals(follow.getAuthorUrl(), "author/newAuthor");
    }

    @Test
    public void it_should_create_new_follow_object_with_no_args_constructor(){

        // Arrange
        Follow follow = new Follow();

        // Assert
        assertNull(follow.getUserName());
        assertNull(follow.getAuthorUrl());
    }

    @Test
    public void it_should_create_new_follow_object_with_builder(){

        // Arrange
        Follow follow = Follow.builder()
                .userName("userName")
                .build();

        // Assert
        assertEquals(follow.getUserName(), "userName");
    }
}
