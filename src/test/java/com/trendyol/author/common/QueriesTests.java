package com.trendyol.author.common;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class QueriesTests {

    //4
    @Test
    public void if_all_parameters_are_full(){
        String query = Queries.searchQuery("Title1","Mahmut", 1603022806972L, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1' AND  a.about.userName = 'Mahmut'  AND  art.publishDate <= 1603022806972  AND  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    //3
    @Test
    public void when_method_gets_title_userNane_publishDate_parameters(){
        String query = Queries.searchQuery("Title1","Mahmut", 1603022806972L, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1' AND  a.about.userName = 'Mahmut'  AND  art.publishDate <= 1603022806972 ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_title_userNane_tag_parameters(){
        String query = Queries.searchQuery("Title1","Mahmut", null, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1' AND  a.about.userName = 'Mahmut'  AND  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_title_publishDate_tag_parameters(){
        String query = Queries.searchQuery("Title1",null, 1603022806972L, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1' AND  art.publishDate <= 1603022806972  AND  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_userName_publishDate_tag_parameters(){
        String query = Queries.searchQuery(null,"Mahmut", 1603022806972L, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  a.about.userName = 'Mahmut'  AND  art.publishDate <= 1603022806972  AND  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    //2
    @Test
    public void when_method_gets_title_and_userName_parameters(){
        String query = Queries.searchQuery("Title1","Mahmut", null, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1' AND  a.about.userName = 'Mahmut' ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_title_and_publishDate_parameters(){
        String query = Queries.searchQuery("Title1",null, 1603022806972L, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1' AND  art.publishDate <= 1603022806972 ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_title_and_tag_parameters(){
        String query = Queries.searchQuery("Title1",null, null, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1' AND  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_userName_and_publishDate_parameters(){
        String query = Queries.searchQuery(null,"Mahmut", 1603022806972L, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  a.about.userName = 'Mahmut'  AND  art.publishDate <= 1603022806972 ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_userName_and_tag_parameters(){
        String query = Queries.searchQuery(null,"Mahmut", null, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  a.about.userName = 'Mahmut'  AND  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_publishDate_and_tag_parameters(){
        String query = Queries.searchQuery(null,null, 1603022806972L, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.publishDate <= 1603022806972  AND  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    //1

    @Test
    public void when_method_gets_title_parameter(){
        String query = Queries.searchQuery("Title1",null, null, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.title = 'Title1'";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_userName_parameter(){
        String query = Queries.searchQuery(null,"Mahmut", null, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  a.about.userName = 'Mahmut' ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_publishDate_parameter(){
        String query = Queries.searchQuery(null,null, 1603022806972L, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  art.publishDate <= 1603022806972 ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void when_method_gets_tag_parameter(){
        String query = Queries.searchQuery(null,null, null, "Python");

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art WHERE  ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), 'Python') END ";

        assertThat(query).isEqualTo(result);
    }

    @Test
    public void all_parameter_is_null(){
        String query = Queries.searchQuery(null,null, null, null);

        String result = "SELECT art.* FROM author AS a UNNEST a.articles AS art ";

        assertThat(query).isEqualTo(result);
    }



}
