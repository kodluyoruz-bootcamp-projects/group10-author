package com.trendyol.author.common.jwt;


import com.trendyol.author.common.exception.JsonParseExceptionWithJackson;
import com.trendyol.author.common.json.Jackson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class JWTGeneratorTest {


    @Test
    public void throw_null_pointer_exception_when_generate_parameter_is_null(){
        //Assert
        Assertions.assertThrows(NullPointerException.class, () ->
                JWTGenerator.generate(null));
    }


    @Test
    public void throw_null_pointer_exception_when_decode_parameter_is_null(){
        //Assert
        Assertions.assertThrows(NullPointerException.class, () ->
                JWTGenerator.decode(null));
    }

    @Test
    public void it_should_decode_jwt_and_equal_to_body() {
        //Arrange
        JWTBody body = new JWTBody("123", "beyter");
        String token = JWTGenerator.generate(body);
        //Act
        JWTBody result = JWTGenerator.decode(token);
        //Assert
        assertEquals(result, body);
    }

}
