package com.trendyol.author.common.json;

import com.trendyol.author.common.exception.JsonParseExceptionWithJackson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;

public class JacksonTests {

    @Test
    public void throw_null_pointer_exception_when_null_parameter(){
        //Assert
        Assertions.assertThrows(NullPointerException.class, () ->
                Jackson.objectConvertToJson(null));

    }
    @Test
    public void throw_json_parse_exception_with_jackson_when_not_object_parameter(){
        //Assert
        Assertions.assertThrows(JsonParseExceptionWithJackson.class, () ->
                Jackson.objectConvertToJson(new Object()));
    }

    @Test
    public void it_should_return_string_when_parameter_is_class_or_object(){
       //Arrange
        List<String> sut = new ArrayList<>();

        //Act
        String  result = Jackson.objectConvertToJson(sut);

        //Assert
        assertEquals("[ ]",result);
    }
}
