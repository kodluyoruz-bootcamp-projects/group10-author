package com.trendyol.author.service.author;


import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.contract.response.coucbase.InterestResponse;
import com.trendyol.author.model.author.About;
import com.trendyol.author.repository.AuthorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthorServiceTests {

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    AuthorService service;


    @Test
    public void throw_author_not_found_exception_when_author_id_not_found_for_find_interest() {
        //Arrange
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, "1");
        when(authorRepository.executeSelect(
                query, About.class
        )).thenThrow(new AuthorNotFoundException(""));


        //Assert
        assertThrows(AuthorNotFoundException.class, () ->{
            service.findInterestsById("1");
        });
    }

    @Test
    public void throw_author_not_found_exception_when_executeSelect_return_empty_list_for_find_interest() {
        //Arrange
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, "1");
        when(authorRepository.executeSelect(
                query, InterestResponse.class
        )).thenReturn(Collections.singletonList(new InterestResponse()));

        //Act
        List<String> strings = service.findInterestsById("1");


        //Assert
        assertNull(strings);
    }

    @Test
    public void it_should_return_author_interest_when_author_id_founded_for_find_interest() {
        //Arrange
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, "2");

        when(authorRepository.executeSelect(
                query, InterestResponse.class
        )).thenReturn(Collections.singletonList(new InterestResponse(new ArrayList<>())));

        //Act
        List<String> strings = service.findInterestsById("2");

        //Assert
        assertNotNull(strings);
    }

    @Test
    public void throw_author_not_found_when_author_id_not_found_for_add_interest(){
        //Arrange
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, "3");
        when(authorRepository.executeSelect(
                query, InterestResponse.class
        )).thenThrow(new AuthorNotFoundException(""));
        //Act

        //Assert
        assertThrows(AuthorNotFoundException.class, () ->{
            service.addInterestToAuthorInterestList("3", Arrays.asList("A", "B", "C"));
        });
    }

    @Test
    public void it_should_add_interests_when_author_found_and_interests_list_not_empty(){
        //Arrange
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, "4");

        when(authorRepository.executeSelect(
                query, InterestResponse.class
        )).thenReturn(Collections.singletonList(new InterestResponse(Arrays.asList("A","B","D"))));

        //Act
        List<String> interests = service.addInterestToAuthorInterestList("4", Arrays.asList("A", "B", "C"));

        //Assert

        assertEquals("C",interests.get(0));
    }

    @Test
    public void  throw_author_not_found_exception_when_author_id_not_found_for_delete_interest(){
        //Arrange
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, "5");

        when(authorRepository.executeSelect(
                query, InterestResponse.class
        )).thenThrow(new AuthorNotFoundException(""));

        //Act


        //Assert
        assertThrows(AuthorNotFoundException.class,()->{
            service.deleteInterestFromAuthorInterestList("5", Arrays.asList("A", "B", "C"));
        });

    }

    @Test
    public void it_should_delete_interest_when_author_id_founded_and_compared_list_not_empty_for_delete_interest(){
        //Arrange
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, "6");

        when(authorRepository.executeSelect(
                query, InterestResponse.class
        )).thenReturn(Collections.singletonList(new InterestResponse(Arrays.asList("A","B","D"))));

        //Act
        List<String> interests = service.deleteInterestFromAuthorInterestList("6", Arrays.asList("A", "B", "C"));

        //Assert
        assertEquals("A",interests.get(0));
        assertEquals("B",interests.get(1));
    }

    @Test
    public void it_should_return_true_when_author_name_found_for_existByName(){

        //Arrange
        when(authorRepository.existByName(
               "TESTER"
        )).thenReturn(true);

        //Act
        boolean found = service.existByName("TESTER");

        //Assert
        assert found;

    }

}
