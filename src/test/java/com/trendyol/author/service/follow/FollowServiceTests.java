package com.trendyol.author.service.follow;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.model.author.Author;
import com.trendyol.author.model.author.Follow;
import com.trendyol.author.repository.AuthorRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FollowServiceTests {

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    FollowService followService;

    @Test
    public void throw_author_not_found_exception_when_author_not_found(){

        when(!authorRepository.existByName("name"))
                .thenThrow(new AuthorNotFoundException("Author Not Found!"));

        Assertions.assertThrows(AuthorNotFoundException.class, () ->
                followService.getUserFollowers("username"));

    }

    @Test
    public void it_should_return_author_followers_when_author_found(){

        // Arrange
        when(authorRepository.existByName("name"))
                .thenReturn(true);

        String query = String.format(Queries.SELECT_FOLLOWERS_WITH_AUTHOR_NAME, "name");
        when(authorRepository.executeSelect(query, Follow.class))
                .thenReturn(Collections.singletonList(new Follow()));

        // Act
        List<Follow> followList = followService.getUserFollowers("name");

        // Assert
        assertNotNull(followList);
    }

    @Test
    public void throw_author_not_found_exception_when_author_not_found_in_get_following(){

        when(!authorRepository.existByName("name"))
                .thenThrow(new AuthorNotFoundException("Author Not Found!"));

        Assertions.assertThrows(AuthorNotFoundException.class, () ->
                followService.getUserFollowings("username"));
    }

    @Test
    public void it_should_return_author_followings_when_author_found(){

        // Arrange
        when(authorRepository.existByName("name"))
                .thenReturn(true);

        String query = String.format(Queries.SELECT_FOLLOWINGS_WITH_AUTHOR_NAME, "name");
        when(authorRepository.executeSelect(query, Follow.class))
                .thenReturn(Collections.singletonList(new Follow()));

        // Act
        List<Follow> followList = followService.getUserFollowings("name");

        // Assert
        assertNotNull(followList);
    }


    @Test
    public void it_should_add_author_followers_when_author_and_user_found() throws URISyntaxException {

        // Arrange
        when(authorRepository.findByName("name"))
                .thenReturn(new Author());
        when(authorRepository.findByName("username"))
                .thenReturn(new Author());

        // Act
        URI uri = followService.followAuthorByFollowUserName("name", "username");

        // Assert
        assertEquals(uri, new URI("followings/username"));
    }


    @Test
    public void it_should_add_author_followings_when_author_and_user_found() {

        // Arrange
        when(authorRepository.findByName("name"))
                .thenReturn(new Author());
        when(authorRepository.findByName("username"))
                .thenReturn(new Author());

        // Act
        followService.unfollowAuthorByFollowUserName("name", "username");
    }
}
