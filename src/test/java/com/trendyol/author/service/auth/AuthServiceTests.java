package com.trendyol.author.service.auth;


import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthAlreadyExistException;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.SignInFailureException;
import com.trendyol.author.common.jwt.JWTGenerator;
import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.contract.request.PatchAuthRequest;
import com.trendyol.author.model.auth.Auth;
import com.trendyol.author.repository.AuthRepository;
import com.trendyol.author.repository.AuthorRepository;
import com.trendyol.author.service.author.AuthorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthServiceTests {
    @Mock
    AuthorRepository authorRepository;

    @Mock
    AuthRepository authRepository;

    @InjectMocks
    AuthService service;

    @Test
    public void it_should_token_name_equal_to_auth_name_when_create_auth() {
        //Arrange
        AddAuthRequest addAuthRequest =
                new AddAuthRequest("TESTER", "test@trendyol.com", "password");
        when(authRepository.existsByNameOrUserName(addAuthRequest)).thenReturn(false);

        //Act
        String token = service.createAuth(addAuthRequest);
        String userName = JWTGenerator.decode(token).getUserName();

        //Assert
        assertEquals("TESTER", userName);
    }

    @Test
    public void throw_auth_already_exist_exception_when_auth_user_name_already_exist_for_createAuh() {
        AddAuthRequest addAuthRequest =
                new AddAuthRequest("TESTER", "test@trendyol.com", "password");
        when(authRepository.existsByNameOrUserName(addAuthRequest)).thenReturn(true);

        assertThrows(AuthAlreadyExistException.class, () -> {
            service.createAuth(addAuthRequest);
        });
    }

    @Test
    public void throw_email_or_password_failure_exception_when_wrong_email_or_password_for_SignIn() {
        //Arrange
        String email = "test@trendyol.com";
        String password = "wrong";
        String query = String.format(Queries.SELECT_AUTH_WITH_NAME_AND_PASSWORD, email, password);
        doThrow(new SignInFailureException("")).when(authRepository).executeQuery(query);

        //Assert
        assertThrows(SignInFailureException.class, () -> {
            service.signIn(email, password);
        });

    }

    @Test
    public void it_should_return_new_token_when_email_and_password_is_correct_for_SignIn() {
        //Arrange
        String email = "test@trendyol.com";
        String password = "correct";
        Auth auth = Auth.builder()._password(password).email(email).id("123").userName("TESTER").build();
        String query = String.format(Queries.SELECT_AUTH_WITH_NAME_AND_PASSWORD, email, password);
        when(authRepository.executeQuery(query)).thenReturn(auth);

        //Act
        String token = service.signIn(email, password);
        String userName = JWTGenerator.decode(token).getUserName();

        //Assert
        assertEquals("TESTER", userName);
    }

    @Test
    public void it_should_return_true_when_email_or_password_is_correct_for_checkEmailAndPassword() {
        //Arrange
        String email = "test@trendyol.com";
        String password = "correct";
        Auth auth = Auth.builder()._password(password).email(email).id("123").userName("TESTER").build();
        String query = String.format(Queries.SELECT_AUTH_WITH_NAME_AND_PASSWORD, email, password);
        when(authRepository.executeQuery(query)).thenReturn(auth);

        //Act
        boolean check = service.checkEmailAndPassword(email, password);

        //Assert
        assert check;

    }

    @Test
    public void throw_sign_in_failure_exception_when_email_or_password_is_wrong_for_checkEmailAndPassword() {
        String email = "test@trendyol.com";
        String password = "wrong";
        String query = String.format(Queries.SELECT_AUTH_WITH_NAME_AND_PASSWORD, email, password);
        doThrow(new SignInFailureException("")).when(authRepository).executeQuery(query);

        //Assert
        assertThrows(SignInFailureException.class, () -> {
            service.checkEmailAndPassword(email, password);
        });
    }

    @Test
    public void throw_document_not_found_exception_when_auth_id_not_found_for_delete() {
        //Arrange
        doThrow(new DocumentNotFoundException(null)).when(authRepository).deleteById("123");

        //Assert
        assertThrows(DocumentNotFoundException.class, () -> {
            service.delete("123");
        });

    }

    @Test
    public void throw_author_not_found_exception_when_author_id_not_found_for_delete() {
        //Arrange
        doThrow(new AuthorNotFoundException("")).when(authorRepository).deleteById("123");

        //Assert
        assertThrows(AuthorNotFoundException.class, () -> {
            service.delete("123");
        });
    }

    @Test
    public void it_should_delete_auth_and_author_when_author_id_find_for_delete(){
        //Arrange
        doNothing().when(authorRepository).deleteById("123");
        doNothing().when(authRepository).deleteById("123");


        service.delete("123");
    }


    @Test
    public void throw_author_not_found_exception_when_auth_id_not_found_for_update() {
        doThrow(new DocumentNotFoundException(null)).when(authRepository).findById("1234");

        //Assert
        assertThrows(AuthorNotFoundException.class, () -> {
            service.updateAuth("1234", new PatchAuthRequest());
        });
    }

    @Test
    public void it_should_nothing_when_auth_id_find_and_patchAuthRequest_is_unique_email_and_userName_for_update() {
        //Arrange
        Auth auth = Auth.builder()._password("password").email("email").id("123").userName("TESTER").build();
        when(authRepository.findById("123")).thenReturn(auth);

        //Act
        service.updateAuth("123",new PatchAuthRequest());
    }

}
