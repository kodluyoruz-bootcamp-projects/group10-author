package com.trendyol.author.service.article;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.contract.request.AddArticleRequest;
import com.trendyol.author.contract.request.PatchArticleRequest;
import com.trendyol.author.contract.response.coucbase.ArticleResponse;
import com.trendyol.author.contract.response.coucbase.ArticlesResponse;
import com.trendyol.author.model.article.Article;
import com.trendyol.author.model.article.Statistic;
import com.trendyol.author.model.author.About;
import com.trendyol.author.repository.AuthorRepository;
import com.trendyol.author.service.interest.InterestService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ArticleServiceTests {

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    ArticleService articleService;
    @Mock
    InterestService interestService;

    @Test
    public void throw_author_not_found_exception_when_author_id_not_found() {
        String query = String.format(Queries.SELECT_ARTICLES_WITH_AUTHOR_ID, "1");

        when(authorRepository.executeSelect(query, About.class))
                .thenThrow(new AuthorNotFoundException("Author Not Found!"));

        Assertions.assertThrows(AuthorNotFoundException.class, () -> articleService.getArticles("1"));
    }

    @Test
    public void return_articles_result_when_author_exist() {
        String query = String.format(Queries.SELECT_ARTICLES_WITH_AUTHOR_ID, "1");

        when(authorRepository.executeSelect(query, ArticlesResponse.class))
                .thenReturn(Collections.singletonList(new ArticlesResponse(new ArrayList<>())));

        List<Article> article = articleService.getArticles("1");

        assertNotEquals(article, null);
    }

    @Test
    public void throw_author_not_found_exception_when_author_not_exist_with_get_article() {

        when(authorRepository.existsById("1")).thenReturn(true);

        Article article = new Article();
        article.setStatistic(new Statistic());
        article.getStatistic().setViews(new ArrayList<>());
        String query = String.format(Queries.SELECT_ONE_ARTICLE_WITH_ARTICLE_ID, "2", "2");
        when(authorRepository.executeSelect(query,ArticleResponse.class))
                .thenReturn(Collections.singletonList(new ArticleResponse(article)));

        Article newArticle = articleService.getArticle("1", "2");

        doNothing().when(authorRepository).executeUpdate(query);

        assertNotEquals(newArticle, null);
    }

    @Test
    public void it_should_return_article_when_user_send_author_and_article_ids() {
        String query = String.format(Queries.SELECT_ARTICLES_WITH_AUTHOR_ID, "1");

        when(authorRepository.executeSelect(query, ArticlesResponse.class))
                .thenReturn(Collections.singletonList(new ArticlesResponse(new ArrayList<>())));

        List<Article> article = articleService.getArticles("1");

        assertNotEquals(article, null);
    }

    @Test
    public void update_article_by_id_when_update_article() {

        Article article = new Article();

        String query = String.format(Queries.UPDATE_ARTICLES_ADD_ARTICLE,
                Jackson.objectConvertToJson(article), "1");

        doNothing().when(authorRepository).executeUpdate(query);

        articleService.updateArticleById("1", article);
    }

    @Test
    public void update_article_by_id_when_delete_article_by_id() {

        String query = String.format(Queries.DELETE_ARTICLE_WITH_ID_AND_AUTHOR_ID, "2", "1");

        doNothing().when(authorRepository).executeUpdate(query);

        articleService.deleteArticleById("1", "2");
    }

    @Test
    public void it_should_return_articles_by_filtering_with_required_parameters(){

        String query = Queries.searchQuery("", "", (long) 5, "");

        when(authorRepository.executeSelect(query, Article.class))
                .thenReturn(Collections.singletonList(new Article()));

        List<Article> article = articleService.getArticlesByFiltering("", "", (long) 5, "");

        assertNotEquals(article, null);
    }

    @Test
    public void it_should_insert_article_when_author_found() throws URISyntaxException {

        // Arrange
        when(authorRepository.existsById("1")).thenReturn(true);
        AddArticleRequest article = new AddArticleRequest();
        List<String> tags = new ArrayList<>();
        tags.add("Sport");
        article.setTags(tags);

        // Act
        URI uri = articleService.insert("1", article);

        // Assert
        assertNotNull(uri);
    }

    @Test
    public void it_should_update_article_when_author_found() throws URISyntaxException {

        // Arrange
        when(authorRepository.existsById("1")).thenReturn(true);

        PatchArticleRequest article = new PatchArticleRequest();
        String query = String.format(Queries.SELECT_ONE_ARTICLE_WITH_ARTICLE_ID, "2", "2");
        when(authorRepository.executeSelect(query,ArticleResponse.class))
                .thenReturn(Collections.singletonList(new ArticleResponse(new Article())));

        // Act
        URI uri = articleService.update("1", "2", new PatchArticleRequest());

        // Assert
        assertNotNull(uri);
    }

    @Test
    public void it_should_delete_article_when_author_found() throws URISyntaxException {

        // Arrange
        when(authorRepository.existsById("1")).thenReturn(true);

        String query = String.format(Queries.SELECT_ONE_ARTICLE_WITH_ARTICLE_ID, "2", "2");
        when(authorRepository.executeSelect(query,ArticleResponse.class))
                .thenReturn(Collections.singletonList(new ArticleResponse(new Article())));


        // Act
        articleService.delete("1", "2");
    }

    @Test
    public void throw_author_not_found_exception_in_article_insert_when_author_id_not_found() {

        when(!authorRepository.existsById("2"))
                .thenThrow(new AuthorNotFoundException(""));

        Assertions.assertThrows(AuthorNotFoundException.class, () -> articleService.insert("1", new AddArticleRequest()));
    }

    @Test
    public void throw_author_not_found_exception_when_update_article_and_author_id_not_found() {

        when(!authorRepository.existsById("2"))
                .thenThrow(new AuthorNotFoundException(""));

        Assertions.assertThrows(AuthorNotFoundException.class, () -> articleService.update("1", "2", new PatchArticleRequest()));
    }

    @Test
    public void throw_author_not_found_exception_when_delete_and_author_id_not_found() {

        when(!authorRepository.existsById("2"))
                .thenThrow(new AuthorNotFoundException(""));

        Assertions.assertThrows(AuthorNotFoundException.class, () -> articleService.delete("1", "2"));
    }

}
