package com.trendyol.author.service.statistic;


import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.model.article.Clap;
import com.trendyol.author.model.article.Statistic;
import com.trendyol.author.repository.AuthorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class StatisticServiceTests {

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    StatisticService service;


    @Test
    public void throw_author_not_found_exception_when_author_id_not_found_for_clapArticle() {
        //Arrange
        doThrow(AuthorNotFoundException.class).when(authorRepository).existsById("123");


        //Assert
        assertThrows(AuthorNotFoundException.class, () -> {
            service.clapArticle("123", "111");
        });

    }

    @Test
    public void throw_article_not_found_exception_when_article_id_not_found_for_clapArticle() {
        //Arrange
        doThrow(ArticleNotFoundExcepiton.class).when(authorRepository).existArticleById("123");

        //Assert
        assertThrows(ArticleNotFoundExcepiton.class, () -> {
            service.clapArticle("111", "123");
        });
    }

    @Test
    public void it_should_do_update_article_and_return_uri_when_author_and_article_id_find_for_clapArticle()
            throws URISyntaxException {
        //Arrange
        when(authorRepository.existsById("123")).thenReturn(true);

        //Act
        URI url = service.clapArticle("123", "123");

        //Assert
        assertEquals("article/123/claps", url.toString());

    }

    @Test
    public void throw_author_not_found_exception_when_author_id_not_found_for_deleteClapFromArticle() {
        //Arrange
        when(authorRepository.existsById("123")).thenReturn(false);

        //Assert
        assertThrows(AuthorNotFoundException.class, () -> {
            service.deleteClapFromArticle("123", "111");
        });

    }

    @Test
    public void throw_article_not_found_exception_when_article_id_not_found_for_deleteClapFromArticle() {
        //Arrange
        doThrow(ArticleNotFoundExcepiton.class).when(authorRepository).existArticleById("123");

        //Assert
        assertThrows(ArticleNotFoundExcepiton.class, () -> {
            service.deleteClapFromArticle("111", "123");
        });
    }

    @Test
    public void it_should_do_update_article_and_return_uri_when_author_and_article_id_find_for_deleteClapFromArticle() {
        //Arrange
        when(authorRepository.existsById("123")).thenReturn(true);

        //Assert
        service.deleteClapFromArticle("123", "444");
    }

    @Test
    public void throw_author_not_found_exception_when_author_id_not_found_for_getArticlesStatistic() {
        //Arrange
        when(authorRepository.existsById("123")).thenReturn(false);

        //Assert
        assertThrows(AuthorNotFoundException.class, () -> {
            service.getArticlesStatistic("123");
        });
    }

    @Test
    public void it_should_return_statistic_list_when_author_id_find_for_getArticleStatistic(){
        //Arrange
        when(authorRepository.existsById("777")).thenReturn(true);
        String query = String.format(Queries.SELECT_STATISTIC_WITH_AUTHOR_ID, "777");
        when(authorRepository.executeSelect(query, Statistic.class)).thenReturn(new ArrayList<>());

        //Act
        List<Statistic> articlesStatistic = service.getArticlesStatistic("777");

        //Assert
        assertNotNull(articlesStatistic);

    }

    @Test
    public void throw_article_not_found_exception_when_article_id_not_found_for_getArticleClapSize() {
        //Arrange
        doThrow(ArticleNotFoundExcepiton.class).when(authorRepository).existArticleById("123");

        //Assert
        assertThrows(ArticleNotFoundExcepiton.class, () -> {
            service.getArticleClapSize( "123");
        });
    }

    @Test
    public void it_should_return_clap_list_when_article_id_find_for_getArticleClapSize(){
        //Arrange
        String query = String.format(Queries.SELECT_ARTICLE_CLAPS_BY_ARTICLE_ID, "888");
        when(authorRepository.executeSelect(query, Clap.class)).thenReturn(new ArrayList<>());

        //Act
        List<Clap> articleClapSize = service.getArticleClapSize("777");

        //Assert
        assertNotNull(articleClapSize);

    }

    @Test
    public void throw_author_not_found_exception_when_author_id_not_found_for_getArticleStatistic() {
        //Arrange
        when(authorRepository.existsById("123")).thenReturn(false);

        //Assert
        assertThrows(AuthorNotFoundException.class, () -> {
            service.getArticleStatistic("123","111");
        });
    }
    @Test
    public void throw_article_not_found_exception_when_article_id_not_found_for_getArticleStatistic() {
        //Arrange
        when(authorRepository.existsById("111")).thenReturn(true);
        doThrow(ArticleNotFoundExcepiton.class).when(authorRepository).existArticleById("123");

        //Assert
        assertThrows(ArticleNotFoundExcepiton.class, () -> {
            service.getArticleStatistic( "111","123");
        });
    }

    @Test
    public void it_should_return_statistic_when_article_and_author_id_find_for_getArticleStatistic(){
        //Arrange
        when(authorRepository.existsById("111")).thenReturn(true);
        String query = String.format(Queries.SELECT_STATISTIC_WITH_ARTICLE_AND_AUTHOR_ID,
                "123", "111");
        when(authorRepository.executeSelect(query, Statistic.class))
                .thenReturn(Collections.singletonList(new Statistic()));

        //Act
        Statistic statistic = service.getArticleStatistic("111", "123");

        //Assert
        assertNotNull(statistic);

    }
}
