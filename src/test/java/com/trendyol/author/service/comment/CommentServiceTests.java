package com.trendyol.author.service.comment;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.CommentNotFounException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.contract.request.AddCommentRequest;
import com.trendyol.author.contract.response.coucbase.CommentsResponse;
import com.trendyol.author.model.article.Comment;

import com.trendyol.author.repository.AuthorRepository;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CommentServiceTests {

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    CommentService service;

    @Test
    public void return_ArticleNotFoundException_when_get_comments() {
        doThrow(new ArticleNotFoundExcepiton("")).when(authorRepository).existArticleById("1");

        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, "1", "1");

        when(authorRepository.executeSelect(query, CommentsResponse.class))
                .thenReturn(new ArrayList<>());

        assertThrows(ArticleNotFoundExcepiton.class, () ->{
            service.getComments("1");
        });
    }

    @Test
    public void get_comments_return_list_comments() {

        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, "2", "2");

        when(authorRepository.executeSelect(query, CommentsResponse.class))
                .thenReturn(Arrays.asList(new CommentsResponse(Arrays.asList(new Comment("2","hi")))));

        List<Comment> comments = service.getComments("2");

        assertNotNull(comments);
    }

    @Test
    public void get_comment_return_comment(){

        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, "3", "3");

        List<CommentsResponse> commentsResponses = new ArrayList<>();

        CommentsResponse commentsResponse = new CommentsResponse();
        Comment comment = new Comment("3","selam");
        String id = comment.getId();
        List<Comment> comments = Arrays.asList(comment);
        commentsResponse.setComments(comments);
        commentsResponses.add(commentsResponse);

        when(authorRepository.executeSelect(query, CommentsResponse.class))
                .thenReturn(commentsResponses);

        assertNotNull(service.getComment("3",id));
    }

    @Test
    public void return_AuthorNotFoundException_when_add_comment(){
        when(!authorRepository.existsById("1"))
                .thenThrow(new AuthorNotFoundException("Author Not Found!"));

        Assertions.assertThrows(AuthorNotFoundException.class, () -> service.addComment("1", new AddCommentRequest()));
    }

    @Test
    public void add_comment(){

        when(!authorRepository.existsById("4"))
                .thenReturn(true);

        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setAuthorId("4");
        addCommentRequest.setComment("merhaba");

        Comment com = new Comment(addCommentRequest.getAuthorId(), addCommentRequest.getComment());
        String object = Jackson.objectConvertToJson(com);

        String query = String.format(Queries.APPEND_COMMENTS_WITH_ARTICLE_ID,
                object, "4", "4");

        doNothing().when(authorRepository).executeUpdate(query);
        service.addComment("4",addCommentRequest);
    }

    @Test
    public void patch_comment(){
        when(!authorRepository.existsById("99"))
                .thenReturn(true);

        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, "99", "99");

        Comment comment = new Comment("99", "merhaba");
        when(authorRepository.executeSelect(query, CommentsResponse.class))
                .thenReturn(Collections.singletonList(new CommentsResponse(Arrays.asList(comment))));

        AddCommentRequest addCommentRequest = new AddCommentRequest();
        addCommentRequest.setComment("merhaba");
        addCommentRequest.setAuthorId("99");

        service.patchComment("99", comment.getId() ,addCommentRequest);
    }

    @Test
    public void return_AuthorNotFoundException_when_patch_comment(){
        when(!authorRepository.existsById("5"))
                .thenThrow(new AuthorNotFoundException("Author Not Found!"));

        Assertions.assertThrows(AuthorNotFoundException.class, () -> service.patchComment("5","5", new AddCommentRequest()));
    }

    @Test
    public void return_AuthorNotFoundException_when_delete_comment(){
        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, "7", "7");

        List<CommentsResponse> commentsResponses = new ArrayList<>();

        CommentsResponse commentsResponse = new CommentsResponse();
        Comment comment = new Comment("7","selam");

        String comentId = comment.getId();

        List<Comment> comments = Arrays.asList(comment);
        commentsResponse.setComments(comments);
        commentsResponses.add(commentsResponse);

        when(authorRepository.executeSelect(query, CommentsResponse.class))
                .thenReturn(commentsResponses);

        assertThrows(AuthorNotFoundException.class, () ->
                service.deleteComment("7","8", comentId));
    }

    @Test
    public void return_CommentNotFounException_when_delete_comment(){
        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, "8", "8");

        List<CommentsResponse> commentsResponses = new ArrayList<>();

        CommentsResponse commentsResponse = new CommentsResponse();
        Comment comment = new Comment("8","selam");

        List<Comment> comments = Arrays.asList(comment);
        commentsResponse.setComments(comments);
        commentsResponses.add(commentsResponse);

        when(authorRepository.executeSelect(query, CommentsResponse.class))
                .thenThrow(new CommentNotFounException("Comment not found"));

        assertThrows(CommentNotFounException.class, () ->
                service.deleteComment("8","8", "7"));
    }

    @Test
    public void delete_comment(){
        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, "10", "10");

        List<CommentsResponse> commentsResponses = new ArrayList<>();

        CommentsResponse commentsResponse = new CommentsResponse();
        Comment comment = new Comment("10","selam");

        String comentId = comment.getId();

        List<Comment> comments = Arrays.asList(comment);
        commentsResponse.setComments(comments);
        commentsResponses.add(commentsResponse);

        when(authorRepository.executeSelect(query, CommentsResponse.class))
                .thenReturn(commentsResponses);

        String object = Jackson.objectConvertToJson(comment);
        String query2 = String.format(Queries.DELETE_COMMENT_WITH_OBJECT, object, "10", "10");

        doNothing().when(authorRepository).executeUpdate(query2);

        service.deleteComment("10","10", comentId);
    }
}
