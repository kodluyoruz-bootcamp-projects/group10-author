package com.trendyol.author.service.interest;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.exception.InterestAlreadyExistException;
import com.trendyol.author.common.exception.InterestNotCreatedException;
import com.trendyol.author.common.exception.InterestNotFoundException;
import com.trendyol.author.model.interest.Interest;
import com.trendyol.author.repository.InterestRepository;
import io.swagger.models.auth.In;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class InterestServiceTests {

    @Mock
    InterestRepository interestRepository;

    @InjectMocks
    InterestService interestService;

    @Test
    public void it_should_return_interest_list_when_get_interest_run(){

        // Arrange
        when(interestRepository.findAll()).thenReturn((new ArrayList<>()));

        // Act
        List<Interest> interest = interestService.findAll();

        // Assert
        assertNotNull(interest);
    }

    @Test
    public void it_should_return_interest_when_interest_id_found(){

        // Arrange
        when(interestRepository.findById("1")).thenReturn((new Interest()));

        // Act
        Interest interest = interestService.findById("1");

        // Assert
        assertNotNull(interest);
    }

    @Test
    public void it_should_throw_interest_not_found_exception_when_interest_id_not_found(){

        // Arrange
        when(interestRepository.findById("2"))
                .thenThrow((new DocumentNotFoundException(null)));

        // Assert
        Assertions.assertThrows(InterestNotFoundException.class, () ->
                interestService.findById("2"));
    }

    @Test
    public void it_should_increment_popularity_when_interest_add(){

        // Arrange
        List<String> names = Arrays.asList("A","B");
        doNothing().when(interestRepository).incrementPopularity(names);

        // Assert
        interestService.incrementPopularity(names);
    }

    @Test
    public void it_should_decrease_popularity_when_interest_deleted_from_a_list(){

        // Arrange
        List<String> names = Arrays.asList("A","B");
        doNothing().when(interestRepository).decreasePopularity(names);

        // Assert
        interestService.decreasePopularity(names);
    }

    @Test
    public void it_should_add_new_interest_when_new_interest_created() throws URISyntaxException {

        // Arrange
        Interest interest = new Interest("name");
        doNothing().when(interestRepository).create("1", interest);

        // Assert
        interestService.addInterest("name");
    }

    @Test
    public void it_should_update_interest_when_interest_updated(){

        // Arrange
        Interest interest = new Interest("name");
        when(interestRepository.findById("1"))
                .thenReturn(interest);
        doNothing().when(interestRepository).updateById("1", interest);

        // Act
        URI uri = interestService.updateInterest("name", "1");

        // Assert
        assertNotNull(uri);
    }

    @Test
    public void it_should_throw_interest_not_found_exception_when_update_interest(){

        // Arrange
        when(interestRepository.findById("1"))
                .thenThrow((new DocumentNotFoundException(null)));
        doNothing().when(interestRepository).updateById("1", new Interest());

        // Assert
        Assertions.assertThrows(InterestNotFoundException.class, () ->
                interestService.updateInterest("name", "1"));
    }

    @Test
    public void it_should_throw_interest_already_exist_exception_when_insert_exist_interest(){

        // Arrange
        List<Interest> interest = new ArrayList<>();
        interest.add(new Interest("Food"));
        interest.add((new Interest("Culture")));

        when(interestRepository.findAll())
                .thenReturn(interest);
        doNothing().when(interestRepository).create("1", new Interest("Food"));

        // Assert
        Assertions.assertThrows(InterestAlreadyExistException.class, () ->
                interestService.addInterest("Food"));
    }

}
