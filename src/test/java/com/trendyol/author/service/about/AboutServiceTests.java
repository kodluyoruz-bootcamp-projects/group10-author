package com.trendyol.author.service.about;


import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.Queries;

import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.model.author.About;
import com.trendyol.author.repository.AuthorRepository;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AboutServiceTests {

    @Mock
    AuthorRepository authorRepository;

    @InjectMocks
    AboutService service;

    @Test
    public void throw_author_not_found_exception_when_author_id_not_found() {
        String query =  String.format(Queries.SELECT_ABOUT_BY_AUTHOR_ID, "1");

        when(authorRepository.executeSelect(
             query  , About.class
        )).thenThrow(new AuthorNotFoundException(""));

        assertThrows(AuthorNotFoundException.class, () ->{
            service.getAbout("1");
        });
    }

    @Test
    public void return_AuthorNotFoundException_when_get_about() {
        String query =  String.format(Queries.SELECT_ABOUT_BY_AUTHOR_ID, "2");

        when(authorRepository.executeSelect(
                query  , About.class
        )).thenReturn(new ArrayList<>());

        assertThrows(AuthorNotFoundException.class, () ->{
            service.getAbout("2");
        });
    }

    @Test
    public void return_about_object_when_get_about() {
        String query =  String.format(Queries.SELECT_ABOUT_BY_AUTHOR_ID, "3");


        when(authorRepository.executeSelect(
                query  , About.class
        )).thenReturn(Arrays.asList(new About(), new About()));

        About about = service.getAbout("3");

        assertNotEquals(about, null);
    }

    @Test
    public void except_AuthorNotFoundException_when_return_DocumentNotFoundException() {
     /*   String query =  String.format(Queries.SELECT_ABOUT_BY_AUTHOR_ID, "4");

        when(authorRepository.executeSelect(
                query, About.class
        )).thenThrow(new DocumentNotFoundException(null));

        assertThrows(AuthorNotFoundException.class, () ->{
            service.getAbout("4");
        });*/
    }

    @Test
    public void update_about(){

        String query =  String.format(Queries.SELECT_ABOUT_BY_AUTHOR_ID, "5");

        when(authorRepository.executeSelect(
                query  , About.class
        )).thenReturn(Arrays.asList(new About(), new About()));

        service.updateAbout("5", new About());
    }

    @Test
    public void return_URISyntaxException_when_get_about(){
        String query =  String.format(Queries.SELECT_ABOUT_BY_AUTHOR_ID, "%3");

        when(authorRepository.executeSelect(
                query  , About.class
        )).thenReturn(Arrays.asList(new About(), new About()));

        assertThrows(AuthorNotFoundException.class, () ->{
            service.updateAbout("%3", new About());
        });
    }

}
