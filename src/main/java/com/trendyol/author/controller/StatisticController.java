package com.trendyol.author.controller;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.model.article.Article;
import com.trendyol.author.model.article.Statistic;
import com.trendyol.author.service.article.ArticleService;
import com.trendyol.author.service.auth.AuthService;
import com.trendyol.author.service.statistic.StatisticService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/author/{author-id}/statistic")
public class StatisticController {



    private final StatisticService statisticService;

    public StatisticController(StatisticService statisticService) {
        this.statisticService = statisticService;
    }


    @GetMapping
    public ResponseEntity<List<Statistic>> getStatisticsList(@RequestHeader(required = false) String token,
                                                             @PathVariable("author-id") String authorId) {

        try {
            List<Statistic> statistic = statisticService.getArticlesStatistic(authorId);
            return ResponseEntity.ok().body(statistic);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/article/{article-id}")
    public ResponseEntity<Statistic> getStatistic(@RequestHeader(required = false) String token,
                                                  @PathVariable("author-id") String authorId,
                                                  @PathVariable("article-id") String articleId) {
        try {
            Statistic statistic = statisticService.getArticleStatistic(authorId,articleId);
            return ResponseEntity.ok().body(statistic);
        } catch (ArticleNotFoundExcepiton | AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
