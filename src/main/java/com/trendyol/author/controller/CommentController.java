package com.trendyol.author.controller;

import com.trendyol.author.common.exception.*;
import com.trendyol.author.contract.request.AddCommentRequest;
import com.trendyol.author.model.article.Comment;
import com.trendyol.author.service.comment.CommentService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/article/{article-id}/comments")
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping
    public ResponseEntity<List<Comment>> getComments(@PathVariable("article-id") String articleId) {
        try {
            return ResponseEntity.ok(commentService.getComments(articleId));
        } catch (ArticleNotFoundExcepiton e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{comment-id}")
    public ResponseEntity<Comment> getComment(@PathVariable("comment-id") String commentId,
                                              @PathVariable("article-id") String articleId) {

        try {
            return ResponseEntity.ok(commentService.getComment(articleId, commentId));
        } catch (ArticleNotFoundExcepiton | CommentNotFounException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<String> addComment(@PathVariable("article-id") String articleId,
                                             @Validated @RequestBody AddCommentRequest addCommentRequest,
                                             @RequestHeader(required = false) String token) {

        try {
            commentService.addComment(articleId, addCommentRequest);
            return ResponseEntity.ok("Comment added");
        } catch (ArticleNotFoundExcepiton | AuthorNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @PatchMapping("/{comment-id}")
    public ResponseEntity<URI> updateComment(@PathVariable("article-id") String articleId,
                                             @Validated @RequestBody AddCommentRequest addCommentRequest,
                                             @PathVariable("comment-id") String commentId,
                                             @RequestHeader(required = false) String token) {
        try {
            commentService.patchComment(articleId, commentId, addCommentRequest);
            return ResponseEntity.ok().build();
        } catch (ArticleNotFoundExcepiton | CommentNotFounException | AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{comment-id}")
    public ResponseEntity<String> deleteComment(@PathVariable("article-id") String articleId,
                                                @PathVariable("comment-id") String commentId,
                                                @RequestHeader(required = false) String token,
                                                @RequestParam String authorId) {
        try {
            commentService.deleteComment(articleId, authorId,commentId);
            return ResponseEntity.ok().build();
        } catch (ArticleNotFoundExcepiton | AuthorNotFoundException | CommentNotFounException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        } catch (CommentNotDeletedException e) {
            return ResponseEntity.status(401).body(e.getMessage());
        }
    }
}
