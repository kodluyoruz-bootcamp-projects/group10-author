package com.trendyol.author.controller;


import com.trendyol.author.model.article.Article;
import com.trendyol.author.service.article.ArticleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleSearchController {

    private final ArticleService articleService;

    public ArticleSearchController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public ResponseEntity<List<Article>> getArticles(@RequestParam(required = false) String title,
                                                     @RequestParam(required = false) String userName,
                                                     @RequestParam(required = false) Long publishedDate,
                                                     @RequestParam(required = false) String tag) {

        List<Article> articles = new ArrayList<>(articleService.getArticlesByFiltering(title, userName, publishedDate, tag));
        return ResponseEntity.ok(articles);
    }
}
