package com.trendyol.author.controller;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.model.author.Follow;
import com.trendyol.author.service.auth.AuthService;
import com.trendyol.author.service.author.AuthorService;
import com.trendyol.author.service.follow.FollowService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/author/{user-name}")
public class FollowController {


    private final AuthorService authorService;
    private final FollowService followService;

    public FollowController(AuthorService authorService,
                            FollowService followService) {

        this.authorService = authorService;
        this.followService = followService;
    }

    @GetMapping("/followers")
    public ResponseEntity<List<Follow>> getAuthorsFollowers(@PathVariable(value = "user-name") String userName) {
        try {
            List<Follow> userFollowers = followService.getUserFollowers(userName);
            return ResponseEntity.ok(userFollowers);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/followings")
    public ResponseEntity<List<Follow>> getAuthorsFollowings(@PathVariable(value = "user-name") String userName) {
        try {
            List<Follow> userFollowers = followService.getUserFollowings(userName);
            return ResponseEntity.ok(userFollowers);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/followings/{follow-user-name}")
    public ResponseEntity<URI> followAuthor(@PathVariable(value = "user-name") String userName,
                                            @PathVariable(value = "follow-user-name") String followUserName,
                                            @RequestHeader String token) {

        try {
            if(followUserName.equals(userName))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

            if (!authorService.existByName(followUserName) || !authorService.existByName(userName))
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

            URI uri = followService.followAuthorByFollowUserName(userName, followUserName);
            return ResponseEntity.status(201).body(uri);

        } catch (DocumentNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (URISyntaxException e){
            return ResponseEntity.status(500).build();
        }
    }

    @DeleteMapping("/followings/{follower-user-name}")
    public ResponseEntity<Void> unfollowAuthor(@PathVariable(value = "user-name") String userName,
                                               @PathVariable(value = "follower-user-name") String followUserName,
                                               @RequestHeader String token) {
        try {

            if(followUserName.equals(userName))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

            if (!authorService.existByName(followUserName) || !authorService.existByName(userName))
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

            followService.unfollowAuthorByFollowUserName(userName, followUserName);
            return ResponseEntity.status(201).build();

        } catch (DocumentNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
