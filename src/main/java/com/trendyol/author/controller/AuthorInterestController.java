package com.trendyol.author.controller;

import com.couchbase.client.core.error.CouchbaseException;
import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.service.auth.AuthService;
import com.trendyol.author.service.author.AuthorService;
import com.trendyol.author.service.interest.InterestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/author/{author-id}/interest")
public class AuthorInterestController {

    private final AuthorService authorService;
    private final InterestService interestService;

    public AuthorInterestController(AuthorService authorService,
                                    InterestService interestService) {

        this.authorService = authorService;
        this.interestService = interestService;
    }

    @GetMapping
    public ResponseEntity<List<String>> getAuthorInterest(@PathVariable(value = "author-id") String authorId) {

        List<String> interests = authorService.findInterestsById(authorId);
        return ResponseEntity.ok(interests);
    }

    @PostMapping
    public ResponseEntity<Void> addInterest(@RequestParam List<String> interestNames,
                                            @PathVariable(value = "author-id") String authorId,
                                            @RequestHeader(required = false) String token) {
        try {
            List<String> names = authorService.addInterestToAuthorInterestList(authorId, interestNames);
            interestService.incrementPopularity(names);
            return ResponseEntity.status(201).build();
        } catch (DocumentNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/{interest-name}")
    public ResponseEntity<Void> deleteInterest(@PathVariable(value = "author-id") String authorId,
                                               @PathVariable(value = "interest-name") List<String> interestNames,
                                               @RequestHeader(required = false) String token) {
        try {
            List<String> names = authorService.deleteInterestFromAuthorInterestList(authorId, interestNames);
            interestService.decreasePopularity(names);
            return ResponseEntity.ok().build();
        } catch (DocumentNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
