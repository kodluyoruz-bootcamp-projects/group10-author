package com.trendyol.author.controller;

import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.contract.request.AddArticleRequest;
import com.trendyol.author.contract.request.PatchArticleRequest;
import com.trendyol.author.model.article.Article;
import com.trendyol.author.service.article.ArticleService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/author/{id}/article")
public class AuthorArticleController {


    private final ArticleService articleService;

    public AuthorArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public ResponseEntity<List<Article>> getAuthorArticles(@PathVariable String id) {

        try {
            List<Article> articles = articleService.getArticles(id);
            return ResponseEntity.ok(articles);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/{article-id}")
    public ResponseEntity<Article> getAuthorArticle(@PathVariable String id,
                                                    @PathVariable(value = "article-id") String articleId) {

        try {
            Article articles = articleService.getArticle(id,articleId);
            return ResponseEntity.ok(articles);
        } catch (AuthorNotFoundException | ArticleNotFoundExcepiton e) {
            return ResponseEntity.notFound().build();
        }

    }

    @PostMapping
    public ResponseEntity<URI> addArticle(@Validated @RequestBody AddArticleRequest articleRequest,
                                          @PathVariable String id,
                                          @RequestHeader(required = false) String token) {

        try {
            URI url = articleService.insert(id, articleRequest);
            return ResponseEntity.status(201).body(url);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (URISyntaxException e) {
            return ResponseEntity.status(500).build();
        }

    }

    @DeleteMapping("/{article-id}")
    public ResponseEntity<Void> deleteArticle(@PathVariable String id,
                                              @PathVariable(value = "article-id") String articleId,
                                              @RequestHeader(required = false) String token) {

        try {
            articleService.delete(id, articleId);
            return ResponseEntity.ok().build();
        } catch (AuthorNotFoundException | ArticleNotFoundExcepiton e) {
            return ResponseEntity.notFound().build();
        }

    }

    @PatchMapping("/{article-id}")
    public ResponseEntity<URI> updateArticle(@Validated @RequestBody PatchArticleRequest patchArticleRequest,
                                             @PathVariable String id,
                                             @PathVariable(value = "article-id") String articleId,
                                             @RequestHeader(required = false) String token) {

        try {
            URI url = articleService.update(id, articleId, patchArticleRequest);
            return ResponseEntity.created(url).build();
        } catch (AuthorNotFoundException | ArticleNotFoundExcepiton e) {
            return ResponseEntity.notFound().build();
        } catch (URISyntaxException e) {
            return ResponseEntity.status(500).build();
        }
    }
}
