package com.trendyol.author.controller;


import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.exception.AuthAlreadyExistException;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.SignInFailureException;
import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.contract.request.PatchAuthRequest;
import com.trendyol.author.service.auth.AuthService;
import com.trendyol.author.service.author.AuthorService;

import org.springframework.http.ResponseEntity;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;

@RestController
@RequestMapping("/auth")
@Validated
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ResponseEntity<String> signUp(@Validated @RequestBody AddAuthRequest addAuthRequest) {

        try {
            String token = authService.createAuth(addAuthRequest);
            return ResponseEntity.status(201).body(token);
        } catch (AuthAlreadyExistException e) {
            return ResponseEntity.status(409).body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<String> signIn(@Email @RequestParam String email,
                                         @RequestParam String password) {

        try {
            return ResponseEntity.ok(authService.signIn(email, password));
        } catch (SignInFailureException e) {
            return ResponseEntity.status(401).body(e.getMessage());
        } catch (DocumentNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }

    @PatchMapping("{auth-id}")
    public ResponseEntity<String> updateUser(@Validated @RequestBody PatchAuthRequest patchAuthRequest,
                                             @Email @RequestParam String email,
                                             @RequestParam String password,
                                             @PathVariable("auth-id") String authId,
                                             @RequestHeader(required = false) String token) {

        try {
            authService.checkEmailAndPassword(email, password);
            authService.updateAuth(authId, patchAuthRequest);
            return ResponseEntity.ok(authId);
        } catch (SignInFailureException e) {
            return ResponseEntity.status(401).body(e.getMessage());
        } catch (DocumentNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }


    @DeleteMapping("{auth-id}")
    public ResponseEntity<String> deleteUser(@Email @RequestParam String email,
                                             @RequestParam String password,
                                             @PathVariable("auth-id") String authId,
                                             @RequestHeader(required = false) String token) {

        try {
            authService.checkEmailAndPassword(email, password);
            authService.delete(authId);
            return ResponseEntity.ok("Auth deleted..");
        } catch (SignInFailureException e) {
            return ResponseEntity.status(401).body(e.getMessage());
        } catch (DocumentNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        }
    }
}
