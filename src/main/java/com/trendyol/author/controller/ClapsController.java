package com.trendyol.author.controller;

import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.service.statistic.StatisticService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/article/{article-id}/claps")
public class ClapsController {

    private final StatisticService statisticService;

    public ClapsController(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @GetMapping
    public ResponseEntity<Integer> getClaps(@PathVariable("article-id") String articleId) {

        try {
            return ResponseEntity.ok(statisticService.getArticleClapSize(articleId).size());
        } catch (ArticleNotFoundExcepiton e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<URI> addClap(@PathVariable("article-id") String articleId,
                                       @RequestHeader(required = false) String token,
                                       @RequestParam String authorId) {
        try {
            URI url = statisticService.clapArticle(authorId, articleId);
            return ResponseEntity.created(url).build();
        } catch (ArticleNotFoundExcepiton | AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (URISyntaxException e) {
            return ResponseEntity.status(500).build();
        }
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteClap(@PathVariable("article-id") String articleId,
                                           @RequestHeader String token,
                                           @RequestParam String authorId) {

        try {
            statisticService.deleteClapFromArticle(authorId, articleId);
            return ResponseEntity.status(200).build();
        } catch (ArticleNotFoundExcepiton | AuthorNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
