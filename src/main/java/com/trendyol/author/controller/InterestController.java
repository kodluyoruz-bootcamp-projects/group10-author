package com.trendyol.author.controller;

import com.couchbase.client.core.error.CouchbaseException;
import com.trendyol.author.common.exception.InterestNotCreatedException;
import com.trendyol.author.common.exception.InterestNotFoundException;
import com.trendyol.author.model.interest.Interest;
import com.trendyol.author.service.interest.InterestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/interest")
public class InterestController {

    private final InterestService interestService;

    public InterestController(InterestService interestService) {
        this.interestService = interestService;
    }

    @GetMapping
    public ResponseEntity<List<Interest>> getInterests() {
        List<Interest> interests = interestService.findAll();
        return ResponseEntity.ok().body(interests);
    }

    @GetMapping("/{interest-id}")
    public ResponseEntity<Interest> getInterest(@PathVariable("interest-id") String interestId) {
        try {
            Interest interest = interestService.findById(interestId);
            return ResponseEntity.ok().body(interest);
        } catch (InterestNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    @PostMapping
    public ResponseEntity<URI> addInterest(@RequestParam String name) {
        try {
            URI uri = interestService.addInterest(name);
            return ResponseEntity.status(201).body(uri);
        } catch (InterestNotCreatedException | URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PatchMapping("/{interest-id}")
    public ResponseEntity<URI> updateInterest(@RequestParam String name,
                                              @PathVariable("interest-id") String interestId) {
        try {
            URI uri = interestService.updateInterest(name, interestId);
            return ResponseEntity.ok().body(uri);
        } catch (InterestNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

}
