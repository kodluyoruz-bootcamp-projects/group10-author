package com.trendyol.author.controller;

import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.JsonParseExceptionWithJackson;
import com.trendyol.author.model.author.About;
import com.trendyol.author.service.about.AboutService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/author/{author-id}/about")
public class AboutController {

    private final AboutService aboutService;

    public AboutController(AboutService aboutService) {
        this.aboutService = aboutService;
    }

    @GetMapping
    public ResponseEntity<About> getAbout(@PathVariable("author-id") String authorId,
                                          @RequestHeader(required = false) String token) {
        try {
            About about = aboutService.getAbout(authorId);
            return ResponseEntity.ok().body(about);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PatchMapping
    public ResponseEntity<URI> updateAbout(@PathVariable("author-id") String authorId,
                                           @RequestBody About about,
                                           @RequestHeader(required = false) String token) {
        try {
            URI uri = aboutService.updateAbout(authorId, about);
            return ResponseEntity.ok().body(uri);
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (JsonParseExceptionWithJackson e) {
            return ResponseEntity.status(500).build();
        }

    }
}
