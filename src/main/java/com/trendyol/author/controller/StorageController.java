package com.trendyol.author.controller;


import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.StorageUploadFailed;
import com.trendyol.author.service.article.ArticleService;
import com.trendyol.author.service.auth.AuthService;
import com.trendyol.author.service.author.AuthorService;
import com.trendyol.author.service.storage.StorageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/storage/{author-id}")
public class StorageController {


    private final StorageService storageService;

    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("/profile")
    public ResponseEntity<String> uploadProfilePhoto(MultipartFile file,
                                                     @RequestHeader(required = false) String token,
                                                     @PathVariable("author-id") String authorId) {

        try {
            storageService.uploadProfileImage(file, authorId);
            return ResponseEntity.status(201).body("Profile photo added");
        } catch (AuthorNotFoundException e) {
            return ResponseEntity.status(404).body(e.getMessage());
        } catch (StorageUploadFailed e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    @PostMapping("/article/{article-id}/featured-image")
    public ResponseEntity<String> uploadArticleFeaturedImage(MultipartFile file,
                                                          @RequestHeader(required = false) String token,
                                                          @PathVariable("author-id") String authorId,
                                                          @PathVariable("article-id") String articleId) {

        try {
            storageService.uploadFeaturedImage(file, authorId,articleId);
            return ResponseEntity.status(201).body("Feature image added");
        } catch (AuthorNotFoundException | ArticleNotFoundExcepiton e) {
            return ResponseEntity.status(404).body(e.getMessage());
        } catch (StorageUploadFailed e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    @PostMapping("/article/{article-id}/content")
    public ResponseEntity<String> uploadArticleContentFile(MultipartFile file,
                                                           @RequestHeader(required = false) String token,
                                                           @PathVariable("author-id") String authorId,
                                                           @PathVariable("article-id") String articleId) {
        try {
            String url = storageService.uploadArticleContentFile(file, authorId,articleId);
            return ResponseEntity.status(201).body(url);
        } catch (AuthorNotFoundException | ArticleNotFoundExcepiton e) {
            return ResponseEntity.status(404).body(e.getMessage());
        } catch (StorageUploadFailed e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

}
