package com.trendyol.author.contract.request;

import com.trendyol.author.common.enums.PublishStatus;
import com.trendyol.author.model.article.Content;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatchArticleRequest {
    @Size(min = 5, max = 40, message="Title must be between 10 and 40 characters")
    @NotEmpty(message = "Title is not null!")
    private String title;
    private Content content;
    private String featuredImageUrl;
    private PublishStatus publishStatus;
    private List<String> tags;
}
