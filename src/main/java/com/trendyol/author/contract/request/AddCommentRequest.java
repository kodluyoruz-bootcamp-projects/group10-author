package com.trendyol.author.contract.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddCommentRequest {

   // @Max(value=120, message="User Name must be between 8 and 30 characters")
    @NotEmpty(message = "Comment is not null!")
    String comment;
    String authorId;
}
