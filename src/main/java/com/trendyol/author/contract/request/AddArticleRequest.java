package com.trendyol.author.contract.request;

import com.trendyol.author.common.enums.PublishStatus;
import com.trendyol.author.model.article.Content;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddArticleRequest {
    @NonNull
    @Size(min = 5, max = 40, message="Title must be between 10 and 40 characters")
    @NotEmpty(message = "Title is not null!")
    private String title;
    private Content content;
    private String featuredImageUrl;
    @NonNull
    private PublishStatus publishStatus;
    private List<String> tags;

}
