package com.trendyol.author.contract.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PatchAuthRequest {

    @Email(message = "Email should be valid")
    private String email;

    @Size(min = 8, max = 30, message="Password must be between 8 and 30 characters")
    @NotEmpty(message = "Password is not null!")
    private String _password;
}
