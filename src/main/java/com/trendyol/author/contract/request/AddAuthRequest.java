package com.trendyol.author.contract.request;


import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddAuthRequest {

    @Size(min = 6, max = 30, message="User Name must be between 8 and 30 characters")
    @NotEmpty(message = "User Name is not null!")
    private String userName;

    @Email(message = "Email should be valid")
    private String email;

    @Size(min = 8, max = 30, message="Password must be between 8 and 30 characters")
    @NotEmpty(message = "Password is not null!")
    private String _password;

}
