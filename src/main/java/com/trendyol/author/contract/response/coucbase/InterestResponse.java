package com.trendyol.author.contract.response.coucbase;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InterestResponse {
    private List<String> interests;
}
