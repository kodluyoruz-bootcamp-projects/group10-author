package com.trendyol.author.contract.response.coucbase;

import com.trendyol.author.model.article.Article;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticlesResponse {
    List<Article> articles;
}
