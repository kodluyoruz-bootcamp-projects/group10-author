package com.trendyol.author.contract.response;

import com.trendyol.author.model.author.Follow;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FollowingResponse {
    List<Follow> followings;
}
