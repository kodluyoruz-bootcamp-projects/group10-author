package com.trendyol.author.contract.response.coucbase;

import com.trendyol.author.model.article.Article;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleResponse{
    Article article;
}