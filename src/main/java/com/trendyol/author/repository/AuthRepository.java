package com.trendyol.author.repository;


import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.query.QueryResult;
import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.SignInFailureException;
import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.model.auth.Auth;
import com.trendyol.author.repository.crud.Crud;
import org.springframework.stereotype.Repository;

@Repository
public class AuthRepository extends Crud<Auth> {

    private final Cluster couchbaseCluster;

    public AuthRepository(Cluster couchbaseCluster, Collection authCollection) {
        super(authCollection);
        this.couchbaseCluster = couchbaseCluster;
    }

    @Override
    public Auth findById(String id) {
        return collection.get(id).contentAs(Auth.class);
    }

    public Auth executeQuery(String query) {
        QueryResult queryResult = couchbaseCluster.query(query);
        if (queryResult.rowsAs(Auth.class).isEmpty())
            throw new SignInFailureException("User or email failure");
        return queryResult.rowsAs(Auth.class).get(0);
    }

    public void clearDb() {
        couchbaseCluster.query(Queries.DELETE_ALL_AUTH);
    }

    public boolean existsByNameOrUserName(AddAuthRequest addAuthRequest) {
        String query = String.format(Queries.EXIST_AUTH_WITH_NAME_AND_EMAIL, addAuthRequest.getUserName()
                , addAuthRequest.getEmail());
        return !couchbaseCluster.query(query).rowsAs(Auth.class).isEmpty();
    }
}
