package com.trendyol.author.repository;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.query.QueryResult;
import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.*;
import com.trendyol.author.contract.response.AboutResponse;
import com.trendyol.author.model.author.Author;
import com.trendyol.author.repository.crud.Crud;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class AuthorRepository extends Crud<Author> {

    private final Cluster couchbaseCluster;

    public AuthorRepository(Cluster couchbaseCluster, Collection authorCollection) {
        super(authorCollection);
        this.couchbaseCluster = couchbaseCluster;
    }

    @Override
    public Author findById(String authorId) {
        try {
            GetResult getResult = collection.get(authorId);
            return getResult.contentAs(Author.class);
        } catch (DocumentNotFoundException e) {
            throw new AuthorNotFoundException("Author not found");
        }
    }

    public <T> List<T> executeSelect(String query, Class<T> target){
        QueryResult queryResult = couchbaseCluster.query(query);
        return queryResult.rowsAs(target);
    }

    public Author findByName(String name) {
        String query = String.format(Queries.SELECT_AUTHOR_WITH_AUTHOR_NAME, name);
        List<Author> authors = executeSelect(query,Author.class);
        if (authors.isEmpty()) throw new AuthorNotFoundException("There is no author by this name");
        return authors.get(0);
    }

    public boolean existByName(String authorName) {
        String query = String.format(Queries.SELECT_ABOUT_WITH_AUTHOR_NAME, authorName);
        return !executeSelect(query,AboutResponse.class).isEmpty();
    }

    public void existArticleById(String articleId) {
        String query = String.format(Queries.ARTICLE_EXIST_WITH_ID, articleId);
        if (executeSelect(query,AboutResponse.class).isEmpty())
            throw new ArticleNotFoundExcepiton("Article not Found");
    }

    public void executeUpdate(String query) {
        couchbaseCluster.query(query);
    }

}
