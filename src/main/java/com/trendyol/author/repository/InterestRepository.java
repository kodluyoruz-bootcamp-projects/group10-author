package com.trendyol.author.repository;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.query.QueryResult;
import com.trendyol.author.common.Queries;
import com.trendyol.author.model.interest.Interest;
import com.trendyol.author.repository.crud.Crud;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InterestRepository extends Crud<Interest> {

    private final Cluster couchbaseCluster;

    public InterestRepository(Cluster couchbaseCluster, Collection interestCollection) {
        super(interestCollection);
        this.couchbaseCluster = couchbaseCluster;
    }

    @Override
    public Interest findById(String id) {
        return collection.get(id).contentAs(Interest.class);
    }

    public Interest findByName(String name) {
        QueryResult queryResult = couchbaseCluster.query(String.format(Queries.SELECT_INTEREST_BY_NAME, name));
        if (queryResult.rowsAs(Interest.class).isEmpty()){
            Interest interest = new Interest(name);
            create(interest.getId(), interest);
            return interest;
        }
        return queryResult.rowsAs(Interest.class).get(0);
    }

    public List<Interest> findAll() {
        QueryResult queryResult = couchbaseCluster.query(Queries.SELECT_ALL_FROM_INTEREST);
        return queryResult.rowsAs(Interest.class);
    }

    public void incrementPopularity(List<String> names) {
        Interest interest;
        for(String name: names){
            interest = findByName(name);
            interest.increasePopularity();
            updateById(interest.getId(), interest);
        }
    }

    public void decreasePopularity(List<String> names) {
        Interest interest;
        for(String name: names){
            interest = findByName(name);
            if(interest.getPopularity() != 0)
                interest.decreasePopularity();
            updateById(interest.getId(), interest);
        }
    }
}