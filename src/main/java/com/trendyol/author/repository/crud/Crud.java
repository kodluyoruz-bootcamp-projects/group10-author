package com.trendyol.author.repository.crud;


import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.model.author.Author;
import lombok.NonNull;


public abstract class Crud<T> implements ICrud<T> {
    protected final Collection collection;

    public Crud(Collection collection) {
        this.collection = collection;
    }


    @Override
    public void create(@NonNull String id, @NonNull T t) {
        collection.insert(id, t);
    }

    @Override
    public void updateById(@NonNull String id, @NonNull T t) {
        collection.replace(id, t);
    }
    @Override
    public void upsert(String id, T t) {
        collection.upsert(id,t);
    }

    @Override
    public void deleteById(@NonNull String id) {
        collection.remove(id);
    }

    @Override
    public boolean existsById(String id) {
        return collection.exists(id).exists();
    }

}
