package com.trendyol.author.repository.crud;

import java.util.List;

public interface ICrud<T> {
    void create(String id, T t);

    void upsert(String id, T t);

    T findById(String id);


    void updateById(String id, T t);

    void deleteById(String id);

    boolean existsById(String id);

}
