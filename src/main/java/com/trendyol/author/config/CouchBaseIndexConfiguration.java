package com.trendyol.author.config;


import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.manager.query.QueryIndexManager;
import com.trendyol.author.common.Queries;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;


@Configuration
public class CouchBaseIndexConfiguration {

    private final Cluster couchbaseCluster;

    public CouchBaseIndexConfiguration(Cluster couchbaseCluster) {
        this.couchbaseCluster = couchbaseCluster;


    }

    @Bean
    public void createIndexes() {
        for (String query : Queries.allIndexQueries()) {
            try {
                couchbaseCluster.query(query);
            } catch (Exception e) {
                // TODO:  index already created
            }
        }
    }

}