package com.trendyol.author.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StorageConfiguration {

    private final StorageProperties storageProperties;

    public StorageConfiguration(StorageProperties storageProperties) {
        this.storageProperties = storageProperties;
    }

    @Bean
    public String MediumStorage() {
        return String.format("%s/%s", storageProperties.getHost(), storageProperties.getBaseEndPoint());
    }

}
