package com.trendyol.author.service.storage;


import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface IStorageService {
    void uploadProfileImage(MultipartFile file, String authorId);
    void uploadFeaturedImage(MultipartFile file, String authorId, String articleId);
    String uploadArticleContentFile(MultipartFile file, String authorId, String articleId);
}
