package com.trendyol.author.service.storage;


import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.StorageUploadFailed;
import com.trendyol.author.contract.request.RequestFile;
import com.trendyol.author.repository.AuthorRepository;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class StorageService implements IStorageService {

    private final AuthorRepository repository;
    private final String mediumStorage;

    public StorageService(AuthorRepository repository, String mediumStorage) {
        this.repository = repository;
        this.mediumStorage = mediumStorage;
    }

    @Override
    public void uploadProfileImage(MultipartFile file, String authorId) {
        if (!repository.existsById(authorId)) throw new AuthorNotFoundException("Author not found");
        String url = uploadFile(file);
        String query = String.format(Queries.UPDATE_PROFILE_PHOTO_WITH_AUTHOR_ID, url, authorId);
        repository.executeUpdate(query);
    }

    @Override
    public void uploadFeaturedImage(MultipartFile file, String authorId, String articleId) {
        if (!repository.existsById(authorId)) throw new AuthorNotFoundException("Author not found");
        repository.existArticleById(articleId);
        String url = uploadFile(file);
        String query = String.format(Queries.UPDATE_FEATURED_IMAGE_PHOTO_WITH_ARTICLE_ID,
                url, articleId, articleId);
        repository.executeUpdate(query);
    }

    @Override
    public String uploadArticleContentFile(MultipartFile file, String authorId, String articleId) {
        if (!repository.existsById(authorId)) throw new AuthorNotFoundException("Author not found");
        repository.existArticleById(articleId);
        return uploadFile(file);
    }

    private String uploadFile(MultipartFile file) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = null;
            String contentType = file.getOriginalFilename();
            assert contentType != null;
            String extension = contentType.substring(contentType.lastIndexOf('.'));
            RequestFile requestFile = new RequestFile(file.getBytes(), extension);
            response = restTemplate
                    .postForEntity(mediumStorage, requestFile, String.class);
            return response.getBody();
        } catch (IOException e) {
            throw new StorageUploadFailed("File not uploaded ");
        }
    }
}
