package com.trendyol.author.service.statistic;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.model.article.Clap;
import com.trendyol.author.model.article.Statistic;
import com.trendyol.author.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class StatisticService implements IStatisticService {

    private final AuthorRepository authorRepository;

    public StatisticService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public URI clapArticle(String authorId, String articleId) throws URISyntaxException {
        authorRepository.existArticleById(articleId);
        if (!authorRepository.existsById(authorId)) throw new AuthorNotFoundException("Author not found");
        String query = String.format(Queries.UPDATE_ARTICLE_CLAP, Jackson.objectConvertToJson(new Clap(authorId)),
                articleId, articleId);
        authorRepository.executeUpdate(query);
        return new URI(String.format("article/%s/claps", articleId));
    }

    @Override
    public void deleteClapFromArticle(String authorId, String articleId) {
        authorRepository.existArticleById(articleId);
        if (!authorRepository.existsById(authorId)) throw new AuthorNotFoundException("Author not found");
        String query = String.format(Queries.DELETE_ARTICLE_CLAP, Jackson.objectConvertToJson(new Clap(authorId)),
                articleId, articleId);
        authorRepository.executeUpdate(query);
    }

    @Override
    public List<Statistic> getArticlesStatistic(String authorId) {
        if (!authorRepository.existsById(authorId)) throw new AuthorNotFoundException("Author not found ");
        String query = String.format(Queries.SELECT_STATISTIC_WITH_AUTHOR_ID, authorId);
        return authorRepository.executeSelect(query, Statistic.class);
    }

    @Override
    public List<Clap> getArticleClapSize(String articleId) {
        authorRepository.existArticleById(articleId);
        String query = String.format(Queries.SELECT_ARTICLE_CLAPS_BY_ARTICLE_ID, articleId);
        return authorRepository.executeSelect(query, Clap.class);
    }

    @Override
    public Statistic getArticleStatistic(String authorId, String articleId) {
        if (!authorRepository.existsById(authorId)) throw new AuthorNotFoundException("Author not found ");
        authorRepository.existArticleById(articleId);
        String query = String.format(Queries.SELECT_STATISTIC_WITH_ARTICLE_AND_AUTHOR_ID,
                articleId, authorId);
        return authorRepository.executeSelect(query, Statistic.class).get(0);
    }
}
