package com.trendyol.author.service.statistic;

import com.trendyol.author.model.article.Clap;
import com.trendyol.author.model.article.Statistic;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public interface IStatisticService {
    URI clapArticle(String authorId, String articleId) throws URISyntaxException;
    void deleteClapFromArticle(String authorId, String articleId);
    List<Statistic> getArticlesStatistic(String authorId);
    List<Clap> getArticleClapSize(String articleId);
    Statistic getArticleStatistic(String authorId, String articleId);
}
