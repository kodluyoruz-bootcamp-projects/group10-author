package com.trendyol.author.service.interest;


import com.trendyol.author.model.interest.Interest;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public interface IInterestService {
    List<Interest> findAll();
    Interest findById(String id);
    URI addInterest(String name) throws URISyntaxException;
    URI updateInterest(String name, String id);
    void incrementPopularity(List<String> names);
    void decreasePopularity(List<String> names);
}
