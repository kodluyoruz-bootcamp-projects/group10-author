package com.trendyol.author.service.interest;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.exception.InterestAlreadyExistException;
import com.trendyol.author.common.exception.InterestNotFoundException;
import com.trendyol.author.model.interest.Interest;
import com.trendyol.author.repository.InterestRepository;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class InterestService implements IInterestService {

    private final InterestRepository interestRepository;

    public InterestService(InterestRepository interestRepository) {
        this.interestRepository = interestRepository;
    }

    @Override
    public List<Interest> findAll() {
        return interestRepository.findAll();
    }

    @Override
    public Interest findById(String id) {
        try {
            return interestRepository.findById(id);
        } catch (DocumentNotFoundException e) {
            throw new InterestNotFoundException(String.format("Interest %s not found!", id));
        }
    }

    @Override
    public URI addInterest(String name) throws URISyntaxException {

        Interest interest = new Interest(name);
        List<Interest> interests = interestRepository.findAll();
        for (Interest interest1 : interests) {
            if (interest1.getName().equals(interest.getName()))
                throw new InterestAlreadyExistException(String.format("Interest %s already exist!", name)); }
        interestRepository.create(interest.getId(), interest);
        return new URI(String.format("/interest/%s", interest.getId()));
    }

    @Override
    public URI updateInterest(String name, String id) {
        try {
            Interest interest = interestRepository.findById(id);
            interest.setName(name);
            interestRepository.updateById(id, interest);
            return new URI(String.format("/interest/%s", id));
        } catch (DocumentNotFoundException | URISyntaxException e) {
            throw new InterestNotFoundException(String.format("Interest %s not found!", id));
        }
    }

    @Override
    public void incrementPopularity(List<String> names) {
        interestRepository.incrementPopularity(names);
    }

    @Override
    public void decreasePopularity(List<String> names) {
        interestRepository.decreasePopularity(names);
    }
}
