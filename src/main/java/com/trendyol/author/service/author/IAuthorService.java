package com.trendyol.author.service.author;

import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.List;

@Service
public interface IAuthorService {
    List<String> findInterestsById(String authorId);
    List<String> addInterestToAuthorInterestList(String authorId, List<String> interestNames);
    List<String> deleteInterestFromAuthorInterestList(String authorId, List<String> interestNames);
    boolean existByName(String authorName);

}
