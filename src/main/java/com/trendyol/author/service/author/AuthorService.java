package com.trendyol.author.service.author;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.exception.InterestAlreadyExistException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.contract.response.coucbase.InterestResponse;
import com.trendyol.author.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorService implements IAuthorService {

    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<String> findInterestsById(String authorId) {
        String query = String.format(Queries.SELECT_ALL_AUTHOR_INTEREST, authorId);
        List<InterestResponse> interests = authorRepository.executeSelect(query,InterestResponse.class);
        if (interests.isEmpty()) throw new AuthorNotFoundException("Author not found");
        return interests.get(0).getInterests();
    }

    @Override
    public List<String> addInterestToAuthorInterestList(String authorId, List<String> interestNames) {
        List<String> authorInterestList = findInterestsById(authorId);
        List<String> compareList = compareInterests(authorInterestList, interestNames);
        String query = String.format(Queries.APPEND_INTEREST_WITH_AUTHOR_ID,
                jsonCompareList(compareList), authorId);
        authorRepository.executeUpdate(query);
        return compareList;
    }

    @Override
    public List<String> deleteInterestFromAuthorInterestList(String authorId, List<String> interestNames) {
        List<String> authorInterestList = findInterestsById(authorId);
        List<String> compareList = compareDeleteInterests(authorInterestList, interestNames);
        String query = String.format(Queries.DELETE_INTEREST_WITH_AUTHOR_ID,
                jsonCompareList(compareList), authorId);
        authorRepository.executeUpdate(query);
        return compareList;
    }

    @Override
    public boolean existByName(String authorName) {
        return authorRepository.existByName(authorName);
    }

    private String jsonCompareList(List<String> compareList) {
        if (compareList.isEmpty()) throw new InterestAlreadyExistException("Interest already exist");
        String JsonInterestNames = Jackson.objectConvertToJson(compareList);
        return JsonInterestNames.substring(1, JsonInterestNames.length() - 1);
    }

    private List<String> compareInterests(List<String> authorInterestList, List<String> interestNames) {
        List<String> comparedList = new ArrayList<>();
        for (String name : interestNames) {
            if (!authorInterestList.contains(name)) comparedList.add(name);
        }
        return comparedList;
    }

    private List<String> compareDeleteInterests(List<String> authorInterestList, List<String> interestNames) {
        List<String> comparedList = new ArrayList<>();
        for (String name : interestNames) {
            if (authorInterestList.contains(name)) comparedList.add(name);
        }
        return comparedList;
    }

}
