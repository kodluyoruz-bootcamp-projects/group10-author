package com.trendyol.author.service.auth;

import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.contract.request.PatchAuthRequest;
import com.trendyol.author.model.auth.Auth;
import org.springframework.stereotype.Service;

@Service
public interface IAuthService {
    String createAuth(AddAuthRequest auth);
    String signIn(String email, String password);
    boolean checkEmailAndPassword(String email, String password);
    void updateAuth(String authId, PatchAuthRequest patchAuthRequest);
    void delete(String authId);
}
