package com.trendyol.author.service.auth;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthAlreadyExistException;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.jwt.JWTBody;
import com.trendyol.author.common.jwt.JWTGenerator;
import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.contract.request.PatchAuthRequest;
import com.trendyol.author.model.auth.Auth;
import com.trendyol.author.model.author.Author;
import com.trendyol.author.repository.AuthRepository;

import com.trendyol.author.repository.AuthorRepository;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements IAuthService {

    private final AuthRepository authRepository;
    private final AuthorRepository authorRepository;

    public AuthService(AuthRepository authRepository, AuthorRepository authorRepository) {
        this.authRepository = authRepository;
        this.authorRepository = authorRepository;
    }

    @Override
    public String createAuth(AddAuthRequest addAuthRequest) {
        if (authRepository.existsByNameOrUserName(addAuthRequest))
            throw new AuthAlreadyExistException("Auth already exist");
        Auth auth = new Auth(addAuthRequest);
        auth.set_password(auth.get_password());
        auth.setToken(JWTGenerator.generate(new JWTBody(auth.getId(), auth.getUserName())));
        authRepository.create(auth.getId(), auth);
        authorRepository.create(auth.getId(), new Author(auth.getId(), auth.getUserName()));
        return auth.getToken();
    }

    @Override
    public String signIn(String email, String password) {
        String query = String.format(Queries.SELECT_AUTH_WITH_NAME_AND_PASSWORD, email, password);
        Auth auth = authRepository.executeQuery(query);
        auth.setToken(JWTGenerator.generate(new JWTBody(auth.getId(), auth.getUserName())));
        authRepository.updateById(auth.getId(), auth);
        return auth.getToken();
    }

    @Override
    public boolean checkEmailAndPassword(String email, String password) {
        String query = String.format(Queries.SELECT_AUTH_WITH_NAME_AND_PASSWORD, email, password);
        Auth auth = authRepository.executeQuery(query);
        return email.equals(auth.getEmail());
    }

    @Override
    public void updateAuth(String authId, PatchAuthRequest patchAuthRequest) {
        try {
            Auth auth = authRepository.findById(authId);
            auth.patch(patchAuthRequest);
            auth.setToken(JWTGenerator.generate(new JWTBody(authId, auth.getUserName())));
            authRepository.updateById(auth.getId(), auth);
        } catch (DocumentNotFoundException e) {
            throw new AuthorNotFoundException("Auth not found!");
        }
    }

    @Override
    public void delete(String authId) {
        authRepository.deleteById(authId);
        authorRepository.deleteById(authId);
    }
}
