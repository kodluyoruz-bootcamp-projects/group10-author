package com.trendyol.author.service.follow;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.model.author.Author;
import com.trendyol.author.model.author.Follow;
import com.trendyol.author.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class FollowService implements IFollowService {

    private final AuthorRepository authorRepository;

    public FollowService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Follow> getUserFollowers(String username) {

        if (!authorRepository.existByName(username))
            throw new AuthorNotFoundException("There is no author by this name");
        String query = String.format(Queries.SELECT_FOLLOWERS_WITH_AUTHOR_NAME, username);
        return authorRepository.executeSelect(query, Follow.class);
    }

    @Override
    public List<Follow> getUserFollowings(String userName) {
        if (!authorRepository.existByName(userName))
            throw new AuthorNotFoundException("There is no author by this name");
        String query = String.format(Queries.SELECT_FOLLOWINGS_WITH_AUTHOR_NAME, userName);
        return authorRepository.executeSelect(query, Follow.class);
    }

    @Override
    public URI followAuthorByFollowUserName(String userName, String followUserName) throws URISyntaxException {

        Author authorFollow = authorRepository.findByName(userName);
        Author authorFollowing = authorRepository.findByName(followUserName);

        Follow following = new Follow(followUserName, String.format("/author/%s", followUserName));
        Follow follower = new Follow(userName, String.format("/author/%s", userName));

        String queryFollowing = String.format(Queries.APPEND_FOLLOWING_WITH_AUTHOR_ID,
                Jackson.objectConvertToJson(following), authorFollow.getId());
        String queryFollower = String.format(Queries.APPEND_FOLLOWER_WITH_AUTHOR_ID,
                Jackson.objectConvertToJson(follower), authorFollowing.getId());

        authorRepository.executeUpdate(queryFollowing);
        authorRepository.executeUpdate(queryFollower);

        return new URI(String.format("followings/%s", followUserName));
    }

    @Override
    public void unfollowAuthorByFollowUserName(String userName, String followUserName) {
        Author authorUnFollow = authorRepository.findByName(userName);
        Author authorUnFollowing = authorRepository.findByName(followUserName);

        String queryUnFollowing = String.format(Queries.REMOVE_FOLLOWING_WITH_AUTHOR_ID, followUserName, authorUnFollow.getId());
        String queryUnFollower = String.format(Queries.REMOVE_FOLLOWER_WITH_AUTHOR_ID, userName, authorUnFollowing.getId());

        authorRepository.executeUpdate(queryUnFollowing);
        authorRepository.executeUpdate(queryUnFollower);
    }
}
