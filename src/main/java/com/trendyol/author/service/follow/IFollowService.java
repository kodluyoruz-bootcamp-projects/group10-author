package com.trendyol.author.service.follow;

import com.trendyol.author.model.author.Follow;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public interface IFollowService {
    List<Follow> getUserFollowers(String username);
    List<Follow> getUserFollowings(String userName);
    URI followAuthorByFollowUserName(String userName, String followUserName) throws URISyntaxException;
    void unfollowAuthorByFollowUserName(String userName, String followUserName);
}
