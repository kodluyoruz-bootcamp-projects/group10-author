package com.trendyol.author.service.comment;

import com.trendyol.author.contract.request.AddCommentRequest;
import com.trendyol.author.model.article.Comment;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public interface ICommentService {
    List<Comment> getComments(String articleId);
    Comment getComment(String articleId, String commentId);
    void addComment(String articleId, AddCommentRequest addCommentRequest);
    void patchComment(String articleId, String commentId, AddCommentRequest addCommentRequest);
    void deleteComment(String articleId, String authorId, String commentId);
}
