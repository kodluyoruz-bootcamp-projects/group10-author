package com.trendyol.author.service.comment;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.*;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.contract.request.AddCommentRequest;
import com.trendyol.author.contract.response.coucbase.CommentsResponse;
import com.trendyol.author.model.article.Comment;
import com.trendyol.author.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService implements ICommentService {

    private final AuthorRepository authorRepository;

    public CommentService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Comment> getComments(String articleId) {
        authorRepository.existArticleById(articleId);
        String query = String.format(Queries.SELECT_COMMENTS_WITH_ARTICLE_ID, articleId, articleId);
        List<CommentsResponse> responses = authorRepository.executeSelect(query, CommentsResponse.class);
        return responses.get(0).getComments();
    }

    @Override
    public Comment getComment(String articleId, String commentId) {
        Comment comment = getComments(articleId).stream()
                .filter(object -> commentId.equals(object.getId()))
                .findFirst().orElse(null);
        if (comment == null) throw new CommentNotFounException("Comment not found");
        return comment;
    }

    @Override
    public void addComment(String articleId, AddCommentRequest addCommentRequest) {
        if (!authorRepository.existsById(addCommentRequest.getAuthorId()))
            throw new AuthorNotFoundException("Author not found");
        authorRepository.existArticleById(articleId);
        Comment com = new Comment(addCommentRequest.getAuthorId(), addCommentRequest.getComment());
        String object = Jackson.objectConvertToJson(com);
        String query = String.format(Queries.APPEND_COMMENTS_WITH_ARTICLE_ID,
                object, articleId, articleId);
        authorRepository.executeUpdate(query);
    }

    @Override
    public void patchComment(String articleId, String commentId, AddCommentRequest addCommentRequest) {
        if (!authorRepository.existsById(addCommentRequest.getAuthorId()))
            throw new AuthorNotFoundException("Author not found");
        authorRepository.existArticleById(articleId);
        Comment comment = getComment(articleId, commentId);
        String oldObject = Jackson.objectConvertToJson(comment);
        comment.patch(addCommentRequest.getComment());
        String object = Jackson.objectConvertToJson(comment);
        String query = String.format(Queries.UPDATE_COMMENTS_WITH_ARTICLE_ID,
                oldObject, object, articleId, articleId);
        authorRepository.executeUpdate(query);
    }

    @Override
    public void deleteComment(String articleId, String authorId, String commentId) {
        Comment comment = getComment(articleId, commentId);
        if (!authorId.equals(comment.getAuthorId())) throw new AuthorNotFoundException("Author not found ");
        String object = Jackson.objectConvertToJson(comment);
        String query = String.format(Queries.DELETE_COMMENT_WITH_OBJECT, object, articleId, articleId);
        authorRepository.executeUpdate(query);
    }
}