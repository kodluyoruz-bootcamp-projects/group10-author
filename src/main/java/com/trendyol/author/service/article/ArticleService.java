package com.trendyol.author.service.article;


import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.ArticleNotFoundExcepiton;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.contract.request.AddArticleRequest;
import com.trendyol.author.contract.request.PatchArticleRequest;
import com.trendyol.author.contract.response.coucbase.ArticleResponse;
import com.trendyol.author.contract.response.coucbase.ArticlesResponse;
import com.trendyol.author.model.article.Article;
import com.trendyol.author.model.article.Statistic;
import com.trendyol.author.repository.AuthorRepository;
import com.trendyol.author.service.interest.InterestService;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

@Service
public class ArticleService implements IArticleService {

    private final AuthorRepository authorRepository;
    private final InterestService interestService;

    public ArticleService(AuthorRepository authorRepository, InterestService interestService) {
        this.authorRepository = authorRepository;
        this.interestService = interestService;
    }

    @Override
    public List<Article> getArticles(String id) {
        String query = String.format(Queries.SELECT_ARTICLES_WITH_AUTHOR_ID, id);
        List<ArticlesResponse> responses = authorRepository.executeSelect(query,ArticlesResponse.class);
        if (responses.isEmpty()) throw new AuthorNotFoundException("There is no author by this id");
        return responses.get(0).getArticles();
    }

    @Override
    public Article getArticle(String id, String articleId) {
        if (!authorRepository.existsById(id)) throw new AuthorNotFoundException("Author not found");
        Article article = findArticleById(articleId);
        viewArticle(articleId, article.getStatistic());
        return article;
    }

    @Override
    public Article findArticleById(String articleId) {
        String query = String.format(Queries.SELECT_ONE_ARTICLE_WITH_ARTICLE_ID, articleId, articleId);
        List<ArticleResponse> responses = authorRepository.executeSelect(query,ArticleResponse.class);
        if (responses.isEmpty()) throw new ArticleNotFoundExcepiton("Article not found by this id");
        return responses.get(0).getArticle();
    }

    @Override
    public void updateArticleById(String id, Article article) {
        String query = String.format(Queries.UPDATE_ARTICLES_ADD_ARTICLE,
                Jackson.objectConvertToJson(article), id);
        authorRepository.executeUpdate(query);
    }

    @Override
    public void deleteArticleById(String id, String articleId) {
        String query = String.format(Queries.DELETE_ARTICLE_WITH_ID_AND_AUTHOR_ID, articleId, id);
        authorRepository.executeUpdate(query);
    }

    @Override
    public URI insert(String id, AddArticleRequest articleRequest) throws URISyntaxException {
        if (!authorRepository.existsById(id)) throw new AuthorNotFoundException("Author not found");
        Article article = new Article(id,articleRequest);
        String query = String.format(Queries.UPDATE_ARTICLES_ADD_ARTICLE, Jackson.objectConvertToJson(article), id);
        authorRepository.executeUpdate(query);
        interestService.incrementPopularity(article.getTags());
        return new URI(String.format("/author/%s/article/%s", id, article.getId()));
    }

    @Override
    public void delete(String id, String articleId) {
        if (!authorRepository.existsById(id)) throw new AuthorNotFoundException("Author not found");
        Article article = findArticleById(articleId);
        deleteArticleById(id, articleId);
        interestService.decreasePopularity(article.getTags());
    }

    @Override
    public URI update(String id, String articleId, PatchArticleRequest articleRequest) throws URISyntaxException {
        if (!authorRepository.existsById(id)) throw new AuthorNotFoundException("Author not found");
        Article article = findArticleById(articleId);
        article.patchArticle(articleRequest);
        delete(id, articleId);
        updateArticleById(id, article);
        interestService.incrementPopularity(article.getTags());
        return new URI(String.format("/author/%s/article/%s", id, articleId));
    }

    @Override
    public List<Article> getArticlesByFiltering(String title, String userName, Long publishedDate, String tag) {
        String query = Queries.searchQuery(title, userName, publishedDate, tag);
        return authorRepository.executeSelect(query, Article.class);
    }

    private void viewArticle(String articleId, Statistic statistic) {
        statistic.getViews().add(new Date().getTime());
        String query = String.format(Queries.UPDATE_STATISTIC_VIEW, Jackson.objectConvertToJson(statistic)
                , articleId, articleId);
        authorRepository.executeUpdate(query);
    }

}
