package com.trendyol.author.service.article;

import com.trendyol.author.contract.request.AddArticleRequest;
import com.trendyol.author.contract.request.PatchArticleRequest;
import com.trendyol.author.model.article.Article;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public interface IArticleService {
    List<Article> getArticles(String id);

    Article getArticle(String id, String articleId);

    Article findArticleById(String articleId);

    void updateArticleById(String id, Article article);

    void deleteArticleById(String id, String articleId);

    URI insert(String id, AddArticleRequest articleRequest) throws URISyntaxException;

    void delete(String id, String articleId);

    URI update(String id, String articleId, PatchArticleRequest articleRequest) throws URISyntaxException;

    List<Article> getArticlesByFiltering(String title, String userName, Long publishedDate, String tag);

}
