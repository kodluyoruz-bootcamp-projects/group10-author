package com.trendyol.author.service.about;

import com.trendyol.author.common.Queries;
import com.trendyol.author.common.exception.AuthorNotFoundException;
import com.trendyol.author.common.json.Jackson;
import com.trendyol.author.model.author.About;
import com.trendyol.author.repository.AuthorRepository;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class AboutService implements IAboutService {

    AuthorRepository authorRepository;

    public AboutService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public About getAbout(String authorId) {
        String query = String.format(Queries.SELECT_ABOUT_BY_AUTHOR_ID, authorId);
        List<About> responses = authorRepository.executeSelect(query, About.class);
        if (responses.isEmpty()) throw new AuthorNotFoundException("There is no author by this id");
        return responses.get(0);
    }

    @Override
    public URI updateAbout(String authorId, About about) {
        try {
            About oldAbout = getAbout(authorId);
            oldAbout.patch(about);
            String query = String.format(Queries.UPDATE_ABOUT_WITH_AUTHOR_ID,
                    Jackson.objectConvertToJson(oldAbout), authorId);
            authorRepository.executeUpdate(query);
            return new URI(String.format("author/%s/about", authorId));
        } catch (URISyntaxException e) {
            throw new AuthorNotFoundException("There is no author by this name");
        }
    }
}
