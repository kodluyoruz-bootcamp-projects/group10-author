package com.trendyol.author.service.about;

import com.trendyol.author.model.author.About;
import org.springframework.stereotype.Service;

import java.net.URI;

@Service
public interface IAboutService {
    About getAbout(String authorId);
    URI updateAbout(String authorId, About about);
}
