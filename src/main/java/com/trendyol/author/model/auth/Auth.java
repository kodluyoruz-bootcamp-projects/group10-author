package com.trendyol.author.model.auth;

import com.trendyol.author.contract.request.AddAuthRequest;
import com.trendyol.author.contract.request.PatchAuthRequest;
import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Auth {

    private String id;
    private String userName;
    private String email;
    private String _password;
    private String token;

    public Auth(AddAuthRequest addAuthRequest) {
        this.id = UUID.randomUUID().toString();
        this.userName = addAuthRequest.getUserName();
        this.email = addAuthRequest.getEmail();
        this._password = addAuthRequest.get_password();
    }

    public void patch(PatchAuthRequest patchAuthRequest) {
        this.email = patchAuthRequest.getEmail() == null ? this.email : patchAuthRequest.getEmail();
        this._password = patchAuthRequest.get_password() == null ? this._password : patchAuthRequest.get_password();
    }

}
