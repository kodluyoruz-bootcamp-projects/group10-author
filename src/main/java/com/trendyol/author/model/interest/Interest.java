package com.trendyol.author.model.interest;

import com.trendyol.author.common.exception.PopularityCannotBeMinusException;
import lombok.*;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Interest {
    private String id;
    private String name;
    private int popularity;

    public Interest(String name){
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.popularity = 0;
    }

    public void increasePopularity(){
        this.popularity++;
    }

    public void   decreasePopularity(){
        if(popularity == 0)
            throw new PopularityCannotBeMinusException("Popularity Cannot Be Minus!");

        this.popularity--;
    }
}
