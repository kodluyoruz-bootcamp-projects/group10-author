package com.trendyol.author.model.author;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SocialMedia {
    private String twitter;
    private String website;
    private String github;
    private String linkedin;

    public SocialMedia patch(SocialMedia socialMedia) {
        this.twitter = socialMedia.twitter == null ? this.twitter : socialMedia.getTwitter();
        this.website = socialMedia.website == null ? this.website : socialMedia.getWebsite();
        this.github = socialMedia.github == null ? this.github : socialMedia.getGithub();
        this.linkedin = socialMedia.linkedin == null ? this.linkedin : socialMedia.getLinkedin();
        return this;
    }
}
