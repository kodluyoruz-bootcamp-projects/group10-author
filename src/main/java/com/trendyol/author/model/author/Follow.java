package com.trendyol.author.model.author;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Follow {
    private String userName;
    private String authorUrl;
}
