package com.trendyol.author.model.author;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class About {
    private String userName;
    private String name;
    private String profileUrl;
    private String profilePhotoUrl;
    private SocialMedia socialMedia;
    private String bio;

    public void patch(About about) {
        this.name = about.getName() == null ? this.name : about.getName();
        this.profileUrl = about.getProfileUrl() == null ? this.profileUrl : about.getProfileUrl();
        this.profilePhotoUrl = about.getProfilePhotoUrl() == null ? this.profilePhotoUrl : about.getProfilePhotoUrl();
        this.socialMedia = about.getSocialMedia() == null ? this.socialMedia : this.socialMedia.patch(about.getSocialMedia());
        this.bio = about.getBio() == null ? this.bio : about.getBio();
    }
}
