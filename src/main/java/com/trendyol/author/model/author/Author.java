package com.trendyol.author.model.author;


import com.trendyol.author.model.article.Article;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    private String id;
    private About about = new About();
    private List<Article> articles = new ArrayList<>();
    private List<String> interests = new ArrayList<>();
    private List<Follow> followers = new ArrayList<>();
    private List<Follow> followings = new ArrayList<>();

    public Author(String id, String userName) {
        this.id = id;
        this.about.setUserName(userName);
    }
}

