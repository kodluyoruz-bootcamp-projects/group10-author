package com.trendyol.author.model.article;

import com.trendyol.author.common.enums.ArticleType;
import com.trendyol.author.model.author.SocialMedia;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Content {
    private ArticleType articleType;
    private String data;

    public Content patch(Content content) {
        this.articleType = content.getArticleType() == null ? this.articleType: content.getArticleType();
        this.data = content.getData() == null ? this.data: content.getData();
        return this;
    }
}
