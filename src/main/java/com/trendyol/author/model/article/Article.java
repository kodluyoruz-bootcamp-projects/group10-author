package com.trendyol.author.model.article;

import com.trendyol.author.common.enums.PublishStatus;
import com.trendyol.author.contract.request.AddArticleRequest;
import com.trendyol.author.contract.request.PatchArticleRequest;
import lombok.*;

import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Article {
    private String id;
    private List<String> authorId;
    private String title;
    private Content content;
    private String featuredImageUrl;
    private List<String> tags;
    private PublishStatus publishStatus;
    private Long publishDate = new Date().getTime();
    private Statistic statistic;
    private List<Clap> claps;
    private List<Comment> comments;

    public Article(String authorId, AddArticleRequest articleRequest) {
        this.id = UUID.randomUUID().toString();
        this.authorId = Arrays.asList(authorId);
        this.content = articleRequest.getContent();
        this.title = articleRequest.getTitle();
        this.featuredImageUrl = articleRequest.getFeaturedImageUrl();
        this.tags = articleRequest.getTags();
        this.publishStatus = articleRequest.getPublishStatus();
        this.claps = new ArrayList<>();
        this.comments = new ArrayList<>();
        this.statistic = new Statistic();
    }

    public void patchArticle(PatchArticleRequest articleRequest) {
        this.content = articleRequest.getContent() == null ? this.content : this.content.patch(articleRequest.getContent());
        this.title = articleRequest.getTitle() == null ? this.title : articleRequest.getTitle();
        this.featuredImageUrl = articleRequest.getFeaturedImageUrl() == null ? this.featuredImageUrl : articleRequest.getFeaturedImageUrl();
        this.tags = articleRequest.getTags() == null ? this.tags : articleRequest.getTags();
        this.publishStatus = articleRequest.getPublishStatus() == null ? this.publishStatus : articleRequest.getPublishStatus();
    }

}
