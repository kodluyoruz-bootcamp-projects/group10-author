package com.trendyol.author.model.article;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Comment {
    private String id;
    private String comment;
    private Long date;
    private String authorId;

    public Comment(String authorId, String comment) {
        this.id = UUID.randomUUID().toString();
        this.authorId = authorId;
        this.comment = comment;
        this.date = new Date().getTime();
    }

    public void patch(String comment) {
        this.comment = comment;
        this.date = new Date().getTime();
    }
}
