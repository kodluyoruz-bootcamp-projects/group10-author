package com.trendyol.author.model.article;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Statistic {
    private List<Long> views= new ArrayList<>();
}
