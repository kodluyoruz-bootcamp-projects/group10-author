package com.trendyol.author.common.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.trendyol.author.common.exception.JsonParseExceptionWithJackson;

public class Jackson {
    public static <T> String objectConvertToJson(T t){
        if (t==null) throw new NullPointerException();
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            return ow.writeValueAsString(t);
        } catch (JsonProcessingException e) {
            throw new JsonParseExceptionWithJackson(e.getMessage());
        }
    }
}
