package com.trendyol.author.common;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Queries {

    public static final String CREATE_PRIMARY_INDEX_AUTHOR_BUCKET = "CREATE PRIMARY INDEX `author_id` ON `author`";

    public static final String CREATE_PRIMARY_INDEX_AUTH_BUCKET = "CREATE PRIMARY INDEX `auth_id` ON `auth`";

    public static final String CREATE_PRIMARY_INDEX_INTEREST_BUCKET = "CREATE PRIMARY INDEX `interest_id` ON `interest`";

    public static final String CREATE_INDEX_AUTHOR_ABOUT_NAME = "CREATE INDEX `adv_about_name` ON `author`((`about`.`name`))";

    public static final String CREATE_INDEX_ARTICLE_TITLE = "CREATE INDEX `adv_DISTINCT_articles_title_articles` " +
            "ON `author`((distinct (array (`t`.`title`) for `t` in `articles` end)),`articles`)";
    public static final String SELECT_STATISTIC_WITH_ARTICLE_AND_AUTHOR_ID = "SELECT  raw  " +
            "(FIRST p FOR p IN articles WHEN p.id = '%s' END).statistic " +
            "FROM author WHERE id='%s'";

    public static List<String> allIndexQueries() {
        List<String> queries = new ArrayList<>();
        queries.add(CREATE_PRIMARY_INDEX_AUTHOR_BUCKET);
        queries.add(CREATE_PRIMARY_INDEX_AUTH_BUCKET);
        queries.add(CREATE_PRIMARY_INDEX_INTEREST_BUCKET);
        queries.add(CREATE_INDEX_AUTHOR_ABOUT_NAME);
        queries.add(CREATE_INDEX_ARTICLE_TITLE);
        return queries;
    }

    public static final String SELECT_AUTHOR_WITH_AUTHOR_NAME = "SELECT id,about,articles,interests,followers,followings " +
            "FROM author WHERE about.userName='%s'";

    public static final String SELECT_FOLLOWERS_WITH_AUTHOR_NAME = "SELECT f.* FROM author as a " +
            "UNNEST a.followers AS f where a.about.userName='%s'";

    public static final String SELECT_FOLLOWINGS_WITH_AUTHOR_NAME = "SELECT f.* FROM author as a " +
            "UNNEST a.followings AS f where a.about.userName='%s'";

    public static final String SELECT_ARTICLES_WITH_AUTHOR_ID = "SELECT articles FROM author " +
            "WHERE id='%s'";

    public static final String SELECT_STATISTIC_WITH_AUTHOR_ID = "SELECT raw art.statistic " +
            "FROM author as a " +
            "unnest a.articles as art where a.id='%s'";

    public static final String SELECT_AUTH_WITH_NAME_AND_PASSWORD =
            "SELECT `auth`.* FROM auth WHERE email='%s' AND _password='%s' ";

    public static final String SELECT_ALL_FROM_INTEREST = "SELECT `interest`.* FROM interest";

    public static final String SELECT_ALL_AUTHOR_INTEREST = "SELECT interests FROM author WHERE id='%s' ";

    public static final String SELECT_INTEREST_BY_NAME = "SELECT `interest`.* FROM interest where name='%s'";

    public static final String SELECT_ABOUT_WITH_AUTHOR_NAME = "SELECT about FROM author WHERE about.userName='%s'";

    public static final String DELETE_ALL_AUTH = "DELETE FROM auth";

    public static final String EXIST_AUTH_WITH_NAME_AND_EMAIL = "SELECT auth FROM auth " +
            "WHERE userName='%s' OR email ='%s'";

    public static final String SELECT_ABOUT_BY_AUTHOR_ID = "SELECT about.* FROM author WHERE id = '%s'";

    public static final String UPDATE_PROFILE_PHOTO_WITH_AUTHOR_ID = "UPDATE author " +
            "SET about.profilePhotoUrl = '%s' WHERE id = '%s' ";

    public static final String UPDATE_FEATURED_IMAGE_PHOTO_WITH_ARTICLE_ID = "UPDATE author AS d " +
            "SET item.featuredImageUrl = '%s' " +
            "FOR item IN d.articles WHEN item.id = '%s' END " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END";
    public static final String SELECT_ONE_ARTICLE_WITH_ARTICLE_ID = "SELECT FIRST p " +
            "FOR p IN articles WHEN p.id = '%s' END " +
            "AS article FROM author " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END;";

    public static final String SELECT_ARTICLE_CLAPS_BY_ARTICLE_ID = "SELECT c.* FROM author as a " +
            "UNNEST a.articles AS art UNNEST art.claps as c WHERE art.id='%s'";

    public static final String UPDATE_ABOUT_WITH_AUTHOR_ID = "UPDATE author AS a " +
            "SET about = %s WHERE a.id = '%s';";

    public static final String UPDATE_STATISTIC_VIEW = "UPDATE author AS d " +
            "SET item.statistic =  %s FOR item IN d.articles WHEN item.id = '%s' END " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END;";

    public static final String UPDATE_ARTICLES_ADD_ARTICLE = "UPDATE author SET articles = " +
            "ARRAY_APPEND (articles,%s)" +
            "WHERE id = '%s' ";

    public static final String ARTICLE_EXIST_WITH_ID = "SELECT id AS id " +
            "FROM author " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END;";

    public static final String DELETE_ARTICLE_WITH_ID_AND_AUTHOR_ID = "UPDATE author as a " +
            "SET articles = ARRAY_REMOVE(articles, art) FOR art IN articles " +
            "WHEN art.id = '%s' " +
            "AND a.id = '%s' END;";

    public static final String APPEND_INTEREST_WITH_AUTHOR_ID = "UPDATE author " +
            "SET interests = ARRAY_DISTINCT(ARRAY_APPEND(interests,%s)) WHERE id = '%s'";

    public static final String DELETE_INTEREST_WITH_AUTHOR_ID = "UPDATE author SET interests = " +
            "ARRAY_REMOVE(interests, %s ) WHERE id = '%s'";

    public static final String UPDATE_ARTICLE_CLAP = "UPDATE author AS d " +
            "SET item.claps = ARRAY_DISTINCT(ARRAY_APPEND(item.claps, %s)) " +
            "FOR item IN d.articles WHEN item.id = '%s' END " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END";

    public static final String DELETE_ARTICLE_CLAP = "UPDATE author AS d " +
            "SET item.claps = ARRAY_REMOVE(item.claps, %s) " +
            "FOR item IN d.articles WHEN item.id = '%s' END " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END";

    public static final String APPEND_FOLLOWER_WITH_AUTHOR_ID = "UPDATE author " +
            "SET followers = ARRAY_DISTINCT(ARRAY_APPEND(followers, %s)) " +
            "WHERE id = '%s';";

    public static final String APPEND_FOLLOWING_WITH_AUTHOR_ID = "UPDATE author " +
            "SET followings = ARRAY_DISTINCT(ARRAY_APPEND(followings, %s)) " +
            "WHERE id = '%s';";

    public static final String REMOVE_FOLLOWER_WITH_AUTHOR_ID = "UPDATE author AS a " +
            "SET followers = ARRAY_REMOVE(followers, f) FOR f IN followers WHEN f.userName = '%s' " +
            " AND a.id = '%s' END;";

    public static final String REMOVE_FOLLOWING_WITH_AUTHOR_ID = "UPDATE author AS a " +
            "SET followings = ARRAY_REMOVE(followings, f) FOR f IN followings WHEN f.userName = '%s' " +
            " AND a.id = '%s' END;";

    public static final String SELECT_COMMENTS_WITH_ARTICLE_ID = "SELECT (FIRST p FOR p IN articles " +
            "WHEN p.id = '%s' END).comments AS comments FROM author " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END;";

    public static final String APPEND_COMMENTS_WITH_ARTICLE_ID = "UPDATE author AS d " +
            "SET item.comments = (ARRAY_APPEND(item.comments, %s)) FOR item IN d.articles WHEN item.id = '%s' END " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END ;";

    public static final String UPDATE_COMMENTS_WITH_ARTICLE_ID = "UPDATE author AS d " +
            "SET item.comments = (ARRAY_REPLACE(item.comments,%s , %s ,1 )) FOR item IN d.articles " +
            "WHEN item.id = '%s' END " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END ;";

    public static final String DELETE_COMMENT_WITH_OBJECT = "UPDATE author AS d " +
            "SET item.comments = (ARRAY_REMOVE(item.comments, %s)) FOR item IN d.articles " +
            "WHEN item.id = '%s' END " +
            "WHERE ANY t IN articles SATISFIES t.id = '%s' END ;";

    public static String searchQuery(String title, String userName, Long publishDate, String tag){
        String base = "SELECT art.* " +
                "FROM author AS a " +
                "UNNEST a.articles AS art " +
                "WHERE ";
        String query = "";

        if (title!=null && title.length()>1){
            query+=" art.title = '" + title + "'";
        }
        if (userName!=null && userName.length()>1){
            if (query.length()>0) query+=" AND ";
            query+=" a.about.userName = '" + userName + "' ";
        }
        if (publishDate != null){
            if (query.length()>0) query+=" AND ";
            query+=" art.publishDate <= " + publishDate + " ";
        }
        if (tag!=null && tag.length()>0){
            if (query.length()>0) query+=" AND ";
            query+=" ANY x IN art.tags SATISFIES ARRAY_CONTAINS(SPLIT(x,','), '" + tag + "') END ";
        }
        if(query.length() == 0)
            return base.substring(0, (base.length() - 6));

        return base+query;
    }
}

