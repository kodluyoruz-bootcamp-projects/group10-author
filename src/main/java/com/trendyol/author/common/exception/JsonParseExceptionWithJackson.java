package com.trendyol.author.common.exception;

public class JsonParseExceptionWithJackson extends RuntimeException {
    public JsonParseExceptionWithJackson(String message) {
        super(message);
    }
}
