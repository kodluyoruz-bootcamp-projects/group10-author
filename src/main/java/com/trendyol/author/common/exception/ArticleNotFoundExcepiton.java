package com.trendyol.author.common.exception;

public class ArticleNotFoundExcepiton extends RuntimeException {
    public ArticleNotFoundExcepiton(String message) {
        super(message);
    }
}
