package com.trendyol.author.common.exception;

public class CommentNotFounException extends RuntimeException {
    public CommentNotFounException(String message) {
        super(message);
    }
}
