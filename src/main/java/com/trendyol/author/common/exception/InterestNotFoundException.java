package com.trendyol.author.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class InterestNotFoundException extends RuntimeException{

    public InterestNotFoundException(String message){
        super(message);
    }
}
