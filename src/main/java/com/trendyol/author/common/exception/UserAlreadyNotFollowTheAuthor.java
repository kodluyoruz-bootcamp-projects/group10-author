package com.trendyol.author.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserAlreadyNotFollowTheAuthor extends RuntimeException {
    public UserAlreadyNotFollowTheAuthor(String message) {
        super(message);
    }
}
