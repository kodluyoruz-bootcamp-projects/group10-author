package com.trendyol.author.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PopularityCannotBeMinusException extends IllegalArgumentException{
    public PopularityCannotBeMinusException(String message){
        super(message);
    }
}
