package com.trendyol.author.common.exception;

public class IdCannotBeNullException extends RuntimeException {
    public IdCannotBeNullException(String message) {
        super(message);
    }
}
