package com.trendyol.author.common.exception;

public class SignInFailureException extends RuntimeException {
    public SignInFailureException(String message) {
        super(message);
    }
}
