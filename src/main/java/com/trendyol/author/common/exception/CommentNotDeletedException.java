package com.trendyol.author.common.exception;

public class CommentNotDeletedException extends RuntimeException {
    public CommentNotDeletedException(String message) {
        super(message);
    }
}
