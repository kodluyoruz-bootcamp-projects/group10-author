package com.trendyol.author.common.exception;

public class AuthorHasNoFollowers extends RuntimeException {
    public AuthorHasNoFollowers(String message) {
        super(message);
    }
}

