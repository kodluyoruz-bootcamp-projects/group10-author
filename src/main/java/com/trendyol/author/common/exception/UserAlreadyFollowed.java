package com.trendyol.author.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserAlreadyFollowed extends RuntimeException {
    public UserAlreadyFollowed(String message) {
        super(message);
    }
}
