package com.trendyol.author.common.exception;

public class StorageUploadFailed extends RuntimeException {
    public StorageUploadFailed(String message) {
        super(message);
    }
}
