package com.trendyol.author.common.exception;


public class AuthAlreadyExistException extends RuntimeException{
    public AuthAlreadyExistException(String message) {
        super(message);
    }
}
