package com.trendyol.author.common.exception;

public class PermissionDeniedForEditCommentException extends RuntimeException {

    public PermissionDeniedForEditCommentException(String message) {
        super(message);
    }
}
