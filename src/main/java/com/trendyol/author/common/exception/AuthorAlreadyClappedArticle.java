package com.trendyol.author.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class AuthorAlreadyClappedArticle extends RuntimeException {
    public AuthorAlreadyClappedArticle(String message) {
        super(message);
    }
}
