package com.trendyol.author.common.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.NonNull;

import java.security.Key;


public class JWTGenerator {
    private static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public static String generate(@NonNull JWTBody body) {
        if (body==null) throw new NullPointerException();
        return Jwts.builder().setSubject(body.toString()).signWith(key).compact();
    }

    public static JWTBody decode(@NonNull String token) {
        if (token==null) throw new NullPointerException();
        String subject = Jwts.parserBuilder().setSigningKey(key).build().
                parseClaimsJws(token).getBody().getSubject();
        JWTBody body = new JWTBody();
        body.setId(subject.substring(subject.indexOf("=")+1, subject.indexOf(",")));
        body.setUserName(subject.substring(subject.lastIndexOf("=")+1, subject.length()-1));
        return body;
    }

}
